package eu.k5.pet.actions;

import eu.k5.pet.dynamic.WorkflowContext;

public interface Action {


	void execute(WorkflowContext context);


}
