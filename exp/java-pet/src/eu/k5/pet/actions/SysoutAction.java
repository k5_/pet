package eu.k5.pet.actions;

import eu.k5.pet.dynamic.WorkflowContext;

public class SysoutAction implements Action {

	public static Action create() {
		return new SysoutAction();
	}

	@Override
	public void execute(WorkflowContext context) {
		System.out.println("Fire");
	}
}
