package eu.k5.pet.builder;

import java.util.function.Supplier;

import eu.k5.pet.actions.Action;
import eu.k5.pet.model.Transition;

public class T {

	private final String name;

	public T(String name) {
		this.name = name;
	}

	public static T named(String name) {
		return new T(name);
	}

	public T in(String string) {
		return this;
	}

	public T out(String string) {
		return this;
	}

	public T action(Supplier<Action> object) {
		return this;
	}

	public Transition build() {
		return new Transition(null, null, null);
	}
}
