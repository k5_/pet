package eu.k5.pet.builder;

import java.util.function.Supplier;

import eu.k5.pet.actions.Action;

public class TransitionBuilder {

	private String name;
	private Supplier<Action> actionSupplier;
	private ProcessBuilder processBuilder;

	public TransitionBuilder(ProcessBuilder processBuilder, String name) {
		this.processBuilder = processBuilder;
		this.name = name;
	}

	public TransitionBuilder action(Supplier<Action> actionSupplier) {
		this.actionSupplier = actionSupplier;
		return this;
	}

	public ProcessBuilder build() {
		return processBuilder;
	}

}
