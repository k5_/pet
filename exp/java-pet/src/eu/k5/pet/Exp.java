package eu.k5.pet;

import java.util.Arrays;

import eu.k5.pet.actions.Action;
import eu.k5.pet.actions.SysoutAction;
import eu.k5.pet.dynamic.Workflow;
import eu.k5.pet.guard.EventTokenNotOkGuard;
import eu.k5.pet.guard.EventTokenOkGuard;
import eu.k5.pet.guard.Guard;
import eu.k5.pet.model.ActionRef;
import eu.k5.pet.model.GuardRef;
import eu.k5.pet.model.PlaceRef;
import eu.k5.pet.model.Process;
import eu.k5.pet.model.Transition;

public class Exp {
	public static void main(String[] args) {

		Process p = createProcess();

		Workflow workflow = new Workflow();
		workflow.addMarking("init", "event", "NOK");

		boolean hasFired = call(workflow, p);
		System.out.println(hasFired);
		System.out.println(workflow.getMarkings());

		hasFired = call(workflow, p);
		System.out.println(hasFired);
		System.out.println(workflow.getMarkings());

		hasFired = call(workflow, p);
		System.out.println(hasFired);
		System.out.println(workflow.getMarkings());

		hasFired = call(workflow, p);
		System.out.println(hasFired);
		System.out.println(workflow.getMarkings());

		hasFired = call(workflow, p);
		System.out.println(hasFired);
		System.out.println(workflow.getMarkings());

		System.out.println(workflow.getHistMarkings());
	}

	private static boolean call(Workflow workflow, Process p) {
		for (Transition t : p.getTransitions()) {
			boolean placesFilled = inputPlacesFilled(workflow, t);
			boolean placesEmpty = outputPlacesEmpty(workflow, t);
			if (placesFilled && placesEmpty) {

				Guard guard = t.getGuard().getGuardSupplier().get();
				if (guard.canPass(workflow)) {
					fire(workflow, t);
					return true;
				}
			}
		}
		return false;
	}

	private static void fire(Workflow workflow, Transition transition) {

		for (PlaceRef input : transition.getInput()) {
			workflow.removeMarking(input);
		}

		Action action = transition.getAction().getFactory().get();
		action.execute(workflow);

		for (PlaceRef output : transition.getOutput()) {
			workflow.addMarking(output.getName());
		}
	}

	private static boolean inputPlacesFilled(Workflow workflow, Transition transition) {
		for (PlaceRef place : transition.getInput()) {
			if (workflow.getPlace(place.getName()) == null) {
				return false;
			}
		}
		return true;
	}

	private static boolean outputPlacesEmpty(Workflow workflow, Transition transition) {
		for (PlaceRef place : transition.getInput()) {
			if (workflow.getPlace(place.getName()) != null) {
				return false;
			}
		}
		return true;
	}

	private static Process createProcess() {
		Transition init = createInit();
		Transition a = createA(init.getOutput().get(0).getName());
		Transition b = createB(a.getOutput().get(0).getName());
		Transition c = createC(b.getOutput().get(0).getName());
		Transition d = createD(b.getOutput().get(0).getName());

		return new Process(Arrays.asList(init, a, b, c, d));

	}

	private static Transition createInit() {
		PlaceRef placeIn = new PlaceRef("init");
		PlaceRef placeOut = new PlaceRef("ain");
		ActionRef action = new ActionRef("sysout", SysoutAction::create);

		return new Transition(Arrays.asList(placeIn), Arrays.asList(placeOut), action);
	}

	private static Transition createC(String in) {

		PlaceRef placeIn = new PlaceRef(in);
		PlaceRef placeOut = new PlaceRef("cout");
		ActionRef action = new ActionRef("sysout", SysoutAction::create);
		GuardRef guard = new GuardRef(EventTokenOkGuard::create);

		return new Transition(Arrays.asList(placeIn), Arrays.asList(placeOut), action, guard);
	}

	private static Transition createD(String in) {

		PlaceRef placeIn = new PlaceRef(in);
		PlaceRef placeOut = new PlaceRef("dout");
		ActionRef action = new ActionRef("sysout", SysoutAction::create);
		GuardRef guard = new GuardRef(EventTokenNotOkGuard::create);

		return new Transition(Arrays.asList(placeIn), Arrays.asList(placeOut), action, guard);

	}

	private static Transition createA(String in) {
		PlaceRef placeIn = new PlaceRef(in);
		PlaceRef placeOut = new PlaceRef("aout");
		ActionRef action = new ActionRef("sysout", SysoutAction::create);

		return new Transition(Arrays.asList(placeIn), Arrays.asList(placeOut), action);
	}

	private static Transition createB(String in) {
		PlaceRef placeIn = new PlaceRef(in);
		PlaceRef placeOut = new PlaceRef("bout");
		ActionRef action = new ActionRef("sysout", SysoutAction::create);

		return new Transition(Arrays.asList(placeIn), Arrays.asList(placeOut), action);
	}
}
