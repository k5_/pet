package eu.k5.pet.model;

import java.util.function.Supplier;

import eu.k5.pet.guard.Guard;

public class GuardRef {

	private final Supplier<Guard> guardSupplier;

	public GuardRef(Supplier<Guard> guardSupplier) {
		this.guardSupplier = guardSupplier;
	}

	public Supplier<Guard> getGuardSupplier() {
		return guardSupplier;
	}

}
