package eu.k5.pet.model;

import java.util.List;

import eu.k5.pet.guard.TrueGuard;

public class Transition {
	private final List<PlaceRef> input;

	private final ActionRef action;

	private final List<PlaceRef> output;

	private final GuardRef guard;

	public Transition(List<PlaceRef> input, List<PlaceRef> output, ActionRef action) {
		this(input, output, action, new GuardRef(TrueGuard::create));
	}

	public Transition(List<PlaceRef> input, List<PlaceRef> output, ActionRef action, GuardRef guard) {
		this.input = input;
		this.output = output;
		this.action = action;
		this.guard = guard;
	}

	public GuardRef getGuard() {
		return guard;
	}

	public List<PlaceRef> getInput() {
		return input;
	}

	public ActionRef getAction() {
		return action;
	}

	public List<PlaceRef> getOutput() {
		return output;
	}

}
