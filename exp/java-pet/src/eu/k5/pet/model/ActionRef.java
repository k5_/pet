package eu.k5.pet.model;

import java.util.function.Supplier;

import eu.k5.pet.actions.Action;

public class ActionRef {
	private final String type;
	private final Supplier<Action> factory;

	public ActionRef(String type, Supplier<Action> factory) {
		this.type = type;
		this.factory = factory;
	}
	



	public Supplier<? extends Action> getFactory() {
		return factory;
	}

	public String getType() {
		return type;
	}

}
