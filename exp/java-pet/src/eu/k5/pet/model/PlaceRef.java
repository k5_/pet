package eu.k5.pet.model;

public class PlaceRef {
	private final String name;

	public PlaceRef(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
