package eu.k5.pet.model;

import java.util.List;

public class Process {
	private final List<Transition> transitions;

	public Process(List<Transition> transitions) {
		this.transitions = transitions;
	}

	public List<Transition> getTransitions() {
		return transitions;
	}

}
