package eu.k5.pet.guard;

import eu.k5.pet.dynamic.WorkflowContext;

public class EventTokenNotOkGuard implements Guard{

	@Override
	public boolean canPass(WorkflowContext context) {
		boolean equals = !"OK".equals(context.getToken("event").getValue());

		System.out.println("!OK guard " + equals);
		return equals;
	}

	
	public static Guard create(){
		return new EventTokenNotOkGuard();
	}
}
