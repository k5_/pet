package eu.k5.pet.guard;

import eu.k5.pet.dynamic.WorkflowContext;

public class TrueGuard implements Guard {

	@Override
	public boolean canPass(WorkflowContext context) {
		return true;
	}

	public static Guard create() {
		return new TrueGuard();
	}
}
