package eu.k5.pet.guard;

import eu.k5.pet.dynamic.WorkflowContext;

public class EventTokenOkGuard implements Guard {

	@Override
	public boolean canPass(WorkflowContext context) {
		boolean equals = "OK".equals(context.getToken("event").getValue());
		System.out.println("ok guard " + equals);

		return equals;
	}

	public static Guard create() {
		return new EventTokenOkGuard();
	}
}
