package eu.k5.pet.guard;

import eu.k5.pet.dynamic.WorkflowContext;

public interface Guard {

	boolean canPass(WorkflowContext context);
}
