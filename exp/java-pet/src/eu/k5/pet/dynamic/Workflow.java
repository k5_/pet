package eu.k5.pet.dynamic;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.k5.pet.model.PlaceRef;

public class Workflow implements WorkflowContext {

	private Map<String, Token> tokens = new HashMap<>();

	private Map<String, Marking> markings = new HashMap<>();

	private List<HistMarking> histMarkings = new ArrayList<>();

	public void addMarking(String place, String tokenName, String tokenValue) {
		Token token = new Token(tokenName, tokenValue);

		Marking marking = new Marking(place, token, LocalDateTime.now());
		tokens.put(token.getName(), token);
		markings.put(place, marking);
	}

	public void addMarking(String place){
		addMarking(place, "event");
	}
	
	
	public void addMarking(String place, String tokenName) {
		Token token = getToken(tokenName);
		Marking marking = new Marking(place, token, LocalDateTime.now());
		markings.put(place, marking);
	}
	
	public Marking getPlace(String place) {
		return markings.get(place);
	}

	public void removeMarking(PlaceRef input) {
		Marking marking = markings.remove(input.getName());

		if (marking != null) {
			histMarkings.add(new HistMarking(marking, LocalDateTime.now()));
		}
	}

	public Map<String, Marking> getMarkings() {
		return markings;
	}

	public List<HistMarking> getHistMarkings() {
		return histMarkings;
	}

	@Override
	public Token getToken(String name) {
		return tokens.get(name);
	}
}
