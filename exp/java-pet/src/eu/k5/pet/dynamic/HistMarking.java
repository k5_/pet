package eu.k5.pet.dynamic;

import java.time.LocalDateTime;

public class HistMarking {
	private final String place;
	private final Token token;
	private final LocalDateTime created;
	private final LocalDateTime removed;

	public HistMarking(Marking marking, LocalDateTime removed) {
		this(marking.getPlace(), marking.getToken(), marking.getCreated(), removed);
	}

	public HistMarking(String place, Token token, LocalDateTime created, LocalDateTime removed) {
		this.place = place;
		this.token = token;
		this.created = created;
		this.removed = removed;
	}

	public String getPlace() {
		return place;
	}

	public Token getToken() {
		return token;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public LocalDateTime getRemoved() {
		return removed;
	}

	@Override
	public String toString() {
		return "HistMarking [place=" + place + ", token=" + token + ", created=" + created + ", removed=" + removed
				+ "]";
	}

}
