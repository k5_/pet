package eu.k5.pet.dynamic;

import java.time.LocalDateTime;

public class Marking {

	private final String place;

	private final Token token;

	private final LocalDateTime created;

	public Marking(String place, Token token, LocalDateTime created) {
		this.place = place;
		this.token = token;
		this.created = created;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public String getPlace() {
		return place;
	}

	public Token getToken() {
		return token;
	}

	@Override
	public String toString() {
		return "Marking [place=" + place + ", token=" + token + ", created=" + created + "]";
	}

}
