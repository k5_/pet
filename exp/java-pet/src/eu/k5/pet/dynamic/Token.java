package eu.k5.pet.dynamic;

public class Token {
	private final String name;
	private final String value;

	public Token(String name, String value) {
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return "Token [name=" + name + ", value=" + value + "]";
	}

}
