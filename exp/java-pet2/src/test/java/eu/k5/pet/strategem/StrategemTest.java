package eu.k5.pet.strategem;

import java.util.Date;
import java.util.UUID;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.k5.pet.execution.Job;
import eu.k5.pet.execution.Signal;
import eu.k5.pet.strategem.components.TaskNet;
import eu.k5.pet.strategem.components.TaskNet.Task;
import eu.k5.pet.strategem.shared.Direction;
import eu.k5.pet.strategem.shared.Layout;
import eu.k5.pet.strategem.shared.Location;
import eu.k5.pet.strategem.shared.PieceType;
import eu.k5.pet.test.AbstractWorkflowTest;
import eu.k5.pet.workflow.Workflow;

public class StrategemTest extends AbstractWorkflowTest {

	@Test
	public void test() {
		String gameKey = UUID.randomUUID().toString();
		String owner = "w_" + UUID.randomUUID().toString();
		String opp = "p_" + UUID.randomUUID().toString();
		StrategemStart start = new StrategemStart();
		start.setGame(gameKey);
		start.setOwner(owner);
		start.setOwnerName("Owner");
		start.setGameName("Game");
		start.setStarted(new Date());
		start.setOpen(true);
		start.setRows(Layout._4x3.getRows());
		start.setColumns(Layout._4x3.getColumns());

		Job job = new Job(gameKey, toJson(start));
		String id = engine.create(job);
		runAll();

		{
			Task key = TaskNet.findTask(engine, id, "ownerSelect", owner);
			engine.signal(id, "ownerSelectsignal", signal(key.getKey(), owner, new Location(0, 0)));
		}
		runAll();

		{
			String key = TaskNet.findTask(engine, id, "join", "").getKey();
			engine.signal(id, "joinsignal", signal(key, "", new Join(opp)));
		}
		runAll();

		{
			String key = TaskNet.findTask(engine, id, "oppSelect", opp).getKey();
			engine.signal(id, "oppSelectsignal", signal(key, opp, new Location(3, 0)));

		}
		runAll();

		{
			String key = TaskNet.findTask(engine, id, "move", owner).getKey();
			System.out.println(key);
			engine.signal(id, "movesignal", signal(key, owner, new Move(new Location(1, 0), Direction.DOWN)));
		}
		runAll();

		{
			Task keyCurrent = TaskNet.findTask(engine, id,  "bothDecideCurrent",owner);
			Task keyOther = TaskNet.findTask(engine, id,  "bothDecideOther",opp);
			System.out.println(keyCurrent);
			System.out.println(keyOther);

			engine.signal(id, keyCurrent.signal(owner, new Decide(PieceType.ROCK, true)));
			engine.signal(id, keyOther.signal(opp, new Decide(PieceType.PAPER, false)));
		}
		runAll();
		{
			String key = TaskNet.findTask(engine, id, opp, "move").getKey();
			System.out.println(key);
			engine.signal(id, "movesignal", signal(key, opp, new Move(new Location(2, 0), Direction.UP)));
		}
		runAll();

		System.out.println(engine.getMarkings(id));

		System.out.println(engine.getMarkings(id, Tokens.EVENT));
		// engine.signal(id, "opponentSelectSignal", asJson(new Location(2,
		// 2)));
	}

	private String signal(String key, String owner, Object data) {

		return asJson(new Signal(key, owner, asJson(data)));
	}

	private ObjectMapper mapper = new ObjectMapper();

	private String toJson(Object object) {
		try {
			return mapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}

	protected Workflow createWorkflow() {
		return Strategem.createWorkflow();
	}
}
