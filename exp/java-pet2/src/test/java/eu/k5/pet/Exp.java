package eu.k5.pet;

import eu.k5.pet.execution.Engine;
import eu.k5.pet.execution.Job;
import eu.k5.pet.execution.Repository;
import eu.k5.pet.guard.TokenValueGuard;
import eu.k5.pet.workflow.Place;
import eu.k5.pet.workflow.TokenType;
import eu.k5.pet.workflow.Transition;
import eu.k5.pet.workflow.TrueGuard;
import eu.k5.pet.workflow.Workflow;

public class Exp {

	public enum ExpTokens implements TokenType {
		EVENT, COUNT, SIGNAL;

		@Override
		public String initValue() {
			// TODO Auto-generated method stub
			return null;
		}
	}

	public static void main(String[] args) throws InterruptedException {

		Workflow workflow = new Workflow();

		Transition transition = new Transition(new TrueGuard(), new InitAction());
		transition.getInput().add(new Place("init", ExpTokens.EVENT));
		transition.getOutput().add(new Place("add", ExpTokens.EVENT));
		transition.getOutput().add(new Place("count", ExpTokens.COUNT));

		Transition transition2 = new Transition(new LtValueGuard(ExpTokens.COUNT, "5"), new AddAction());
		transition2.getInput().add(new Place("add", ExpTokens.EVENT));
		transition2.getInput().add(new Place("count", ExpTokens.COUNT));
		transition2.getOutput().add(new Place("add", ExpTokens.EVENT));
		transition2.getOutput().add(new Place("count", ExpTokens.COUNT));

		Transition signal = new Transition(new TokenValueGuard("count", "5"), new SignalAction());
		signal.getInput().add(new Place("count", ExpTokens.COUNT));
		signal.getInput().add(new Place("signal", ExpTokens.SIGNAL));

		workflow.addTransition(transition, transition2, signal);

		Engine engine = Engine.builder().withRepository(new Repository()).withWorkflow(workflow).build();
		System.out.println(engine.create(new Job("test", "")));

		System.out.println(engine.create(new Job("test", "")));

		System.out.println(engine.create(new Job("test", "")));

		System.out.println(engine.create(new Job("test", "")));

		Thread.sleep(1_000l);

		engine.signal("ID_4", "signal", "");
		engine.signal("ID_3", "signal", "");
		engine.signal("ID_2", "signal", "");
	}
}
