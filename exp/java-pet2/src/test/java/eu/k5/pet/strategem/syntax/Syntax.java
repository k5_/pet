package eu.k5.pet.strategem.syntax;

import eu.k5.pet.strategem.SelectFlagAction;
import eu.k5.pet.strategem.shared.Player;
import eu.k5.pet.workflow.Place;

public class Syntax extends WorkflowSyntax {

	public Place init() {
		return place(Tokens.INIT);
	}

	public Transition start() {
		return Transition(input(init()), output(ownerSelect(), opponentSelect())

		);
	}

	public Transition doOwnerSelect() {
		return Transition().action(new SelectFlagAction(Player.OWNER)).input(ownerSelect(), ownerSelectSignal())
				.output(ownerSelected());
	}

	public Transition doOpponentSelect() {
return null;
	}

	public Place ownerSelected() {
		return place(Tokens.EVENT);
	}

	public Place ownerSelectSignal() {
		return place(Tokens.SELECT);
	}

	public Place opponentSelect() {
		return place(Tokens.SELECT);
	}

	public Place ownerSelect() {
		return place(Tokens.EVENT);
	}

}
