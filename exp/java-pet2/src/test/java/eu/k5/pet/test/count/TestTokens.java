package eu.k5.pet.test.count;

import java.util.UUID;

import eu.k5.pet.workflow.TokenType;

public enum TestTokens implements TokenType {
	INIT, EVENT, COUNT, SIGNAL_CHARGE{
		@Override
		public String initValue() {
			return UUID.randomUUID().toString();
		}
	}, SIGNAL;
	
	@Override
	public String initValue() {
		return "";
	}
}