package eu.k5.pet.test;

import eu.k5.pet.workflow.Guard;
import eu.k5.pet.workflow.GuardContext;

public class NegateGuard implements Guard {
	private final Guard innner;

	public NegateGuard(Guard innner) {
		this.innner = innner;
	}

	@Override
	public boolean canPass(GuardContext context) {
		return !innner.canPass(context);
	}

}
