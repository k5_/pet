package eu.k5.pet;

import java.util.List;

import eu.k5.pet.execution.Marking;
import eu.k5.pet.workflow.Action;
import eu.k5.pet.workflow.ActionContext;

public class InitAction implements Action {

	@Override
	public void execute(ActionContext context) {
		System.out.println("Init");
		
		List<Marking> markings = context.getInput();

		Marking marking = markings.get(0);

		String value = marking.getValue();

		context.setOutput("count", "count", "0");

	}

}
