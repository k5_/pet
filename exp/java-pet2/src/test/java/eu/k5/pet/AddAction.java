package eu.k5.pet;

import java.util.Optional;

import eu.k5.pet.Exp.ExpTokens;
import eu.k5.pet.execution.Marking;
import eu.k5.pet.workflow.Action;
import eu.k5.pet.workflow.ActionContext;

public class AddAction implements Action{

	@Override
	public void execute(ActionContext context) {
		
		Marking counter = context.getInputPlace(ExpTokens.COUNT);
				
		int value = Integer.valueOf(counter.getValue());
		System.out.println("add " + value + " " + context.getExecutionId());
		
		context.setOutput(ExpTokens.COUNT,  "" + (value+1));
		
	}

	
}
