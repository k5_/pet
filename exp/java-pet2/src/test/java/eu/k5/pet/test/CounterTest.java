package eu.k5.pet.test;

import java.time.Clock;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import eu.k5.pet.Exp.ExpTokens;
import eu.k5.pet.LtValueGuard;
import eu.k5.pet.execution.Engine;
import eu.k5.pet.execution.Job;
import eu.k5.pet.execution.Marking;
import eu.k5.pet.execution.Repository;
import eu.k5.pet.execution.TaskScheduler;
import eu.k5.pet.test.count.AddAction;
import eu.k5.pet.test.count.InitAction;
import eu.k5.pet.test.count.TestTokens;
import eu.k5.pet.workflow.Place;
import eu.k5.pet.workflow.Transition;
import eu.k5.pet.workflow.TrueGuard;
import eu.k5.pet.workflow.Workflow;

public class CounterTest extends AbstractWorkflowTest {

	@Test
	public void test() throws InterruptedException {
		String id = engine.create(new Job("test", "xx"));
		Thread.sleep(100l);

		runAll();

		List<Marking> markings = engine.getMarkings(id, TestTokens.COUNT);
		Assert.assertEquals(1, markings.size());

		Assert.assertEquals("5", markings.get(0).getValue());

	}

	@After
	public void tearDown() throws InterruptedException {
		// engine.shutdown();
	}

	@Override
	protected Workflow createWorkflow() {
		Transition transition = new Transition(new TrueGuard(), new InitAction());
		transition.getInput().add(new Place("init", TestTokens.INIT));
		transition.getOutput().add(new Place("add", TestTokens.EVENT));
		transition.getOutput().add(new Place("count", TestTokens.COUNT));

		Transition transition2 = new Transition(new LtValueGuard(TestTokens.COUNT, "5"), new AddAction());
		transition2.getInput().add(new Place("add", TestTokens.EVENT));
		transition2.getInput().add(new Place("count", TestTokens.COUNT));
		transition2.getOutput().add(new Place("add", TestTokens.EVENT));
		transition2.getOutput().add(new Place("count", TestTokens.COUNT));

		Workflow workflow = new Workflow();
		workflow.addTransition(transition, transition2);
		return workflow;
	}
}
