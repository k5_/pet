package eu.k5.pet.test;

import java.time.Instant;
import java.util.concurrent.TimeUnit;

import eu.k5.pet.execution.Marking;
import eu.k5.pet.workflow.Guard;
import eu.k5.pet.workflow.GuardContext;
import eu.k5.pet.workflow.TokenType;

public class TimeoutGuard implements Guard {

	private TokenType token;
	private long millis;

	public TimeoutGuard(TokenType token, TimeUnit timeunit, int units) {
		this.token = token;
		this.millis = timeunit.toMillis(units);
	}

	@Override
	public boolean canPass(GuardContext context) {

		Instant marking = context.getMarking(token).getCreated().plusMillis(millis);
		Instant now = context.getClock().instant();
		return marking.equals(now) || marking.isBefore(now);
	}

	@Override
	public Instant retest(GuardContext context) {
		Marking marking = context.getMarking(token);

		return marking.getCreated().plusMillis(millis);
	}

}
