package eu.k5.pet.test;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;

public class FixedClock extends Clock {

	private ZoneId zoneId = ZoneId.systemDefault();

	private Instant instant;

	public FixedClock(Instant instant) {
		this.instant = instant;
	}

	@Override
	public ZoneId getZone() {
		return zoneId;
	}

	@Override
	public Instant instant() {
		return instant;
	}

	public void setInstant(Instant instant) {
		this.instant = instant;
	}

	@Override
	public Clock withZone(ZoneId zone) {
		// TODO Auto-generated method stub
		return null;
	}

}
