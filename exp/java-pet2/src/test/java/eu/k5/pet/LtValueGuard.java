package eu.k5.pet;

import eu.k5.pet.execution.Marking;
import eu.k5.pet.execution.Token;
import eu.k5.pet.workflow.Guard;
import eu.k5.pet.workflow.GuardContext;
import eu.k5.pet.workflow.TokenType;

public class LtValueGuard implements Guard {
	private TokenType tokenName;

	private String value;

	public LtValueGuard(TokenType tokenName, String value) {
		this.tokenName = tokenName;
		this.value = value;
	}

	@Override
	public boolean canPass(GuardContext context) {
		Marking marking = context.getMarking(tokenName);
		return value.compareTo(marking.getValue()) > 0;
	}

}
