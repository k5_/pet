package eu.k5.pet.test;

import java.time.Clock;

import org.junit.Before;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.k5.pet.execution.Engine;
import eu.k5.pet.execution.Repository;
import eu.k5.pet.execution.TaskScheduler;
import eu.k5.pet.workflow.Workflow;

public abstract class AbstractWorkflowTest {
	private final ObjectMapper mapper = new ObjectMapper();
	private Repository repository;

	protected Engine engine;

	private TaskScheduler scheduler;

	private TestScheduleExecutorService executor;

	@Before
	public void init() {
		repository = new Repository();
		executor = new TestScheduleExecutorService(Clock.systemUTC());
		scheduler = new TaskScheduler(Clock.systemUTC(), executor);
		engine = Engine.builder().withRepository(repository).withWorkflow(createWorkflow()).withScheduler(scheduler)
				.build();
	}

	protected void runAll() {
		executor.runAll();
	}

	protected String asJson(Object object) {
		try {
			return mapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}

	protected abstract Workflow createWorkflow();

}
