package eu.k5.pet.test;

import java.time.Clock;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class TestScheduleExecutorService implements ScheduledExecutorService {

	private final Clock clock;

	private final List<Runner> runners = new ArrayList<>();

	private abstract class Runner {

		private Instant after;

		public Runner(Instant after) {
			this.after = after;
		}

		public abstract void run();

		public boolean canRun(Clock clock) {
			Instant now = clock.instant();
			return now.equals(after) || now.isAfter(after);
		}

	}

	private class RunnableRunner extends Runner {

		private final Runnable runnable;

		public RunnableRunner(Instant after, Runnable runnable) {
			super(after);
			this.runnable = runnable;
		}

		public RunnableRunner(Runnable runnable) {
			this(clock.instant(), runnable);
		}

		public void run() {
			runnable.run();
		}

	}

	public TestScheduleExecutorService(Clock clock) {
		this.clock = clock;
	}

	@Override
	public boolean awaitTermination(long arg0, TimeUnit arg1) throws InterruptedException {
		throw new UnsupportedOperationException();
	}

	@Override
	public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> arg0) throws InterruptedException {
		throw new UnsupportedOperationException();
	}

	@Override
	public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> arg0, long arg1, TimeUnit arg2)
			throws InterruptedException {
		throw new UnsupportedOperationException();
	}

	@Override
	public <T> T invokeAny(Collection<? extends Callable<T>> arg0) throws InterruptedException, ExecutionException {
		throw new UnsupportedOperationException();
	}

	@Override
	public <T> T invokeAny(Collection<? extends Callable<T>> arg0, long arg1, TimeUnit arg2)
			throws InterruptedException, ExecutionException, TimeoutException {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isShutdown() {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isTerminated() {
		throw new UnsupportedOperationException();

	}

	@Override
	public void shutdown() {
		throw new UnsupportedOperationException();

	}

	@Override
	public List<Runnable> shutdownNow() {
		throw new UnsupportedOperationException();

	}

	@Override
	public <T> Future<T> submit(Callable<T> arg0) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Future<?> submit(Runnable runnable) {
		runners.add(new RunnableRunner(runnable));
		return null;
	}

	@Override
	public <T> Future<T> submit(Runnable arg0, T arg1) {
		throw new UnsupportedOperationException();

	}

	@Override
	public void execute(Runnable arg0) {
		throw new UnsupportedOperationException();

	}

	@Override
	public ScheduledFuture<?> schedule(Runnable command, long delay, TimeUnit unit) {
		RunnableRunner runner = new RunnableRunner(clock.instant().plusMillis(delay), command);
		runners.add(runner);
		return null;

	}

	@Override
	public <V> ScheduledFuture<V> schedule(Callable<V> callable, long delay, TimeUnit unit) {
		throw new UnsupportedOperationException();

	}

	@Override
	public ScheduledFuture<?> scheduleAtFixedRate(Runnable command, long initialDelay, long period, TimeUnit unit) {
		throw new UnsupportedOperationException();

	}

	@Override
	public ScheduledFuture<?> scheduleWithFixedDelay(Runnable command, long initialDelay, long delay, TimeUnit unit) {
		throw new UnsupportedOperationException();

	}

	public void runAll() {

		List<Runner> working = new ArrayList<>(runners);
		runners.clear();
		for (Runner runner : working) {
			if (runner.canRun(clock)) {
				runner.run();
			} else {
				runners.add(runner);
			}
		}
	}

}
