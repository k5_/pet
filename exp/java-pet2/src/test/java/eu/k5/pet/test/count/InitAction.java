package eu.k5.pet.test.count;

import eu.k5.pet.workflow.Action;
import eu.k5.pet.workflow.ActionContext;

public class InitAction implements Action {
	@Override
	public void execute(ActionContext context) {
		context.setOutput(TestTokens.COUNT, "0");
	}
}