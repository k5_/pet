package eu.k5.pet.test;

import java.time.Clock;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import eu.k5.pet.execution.Engine;
import eu.k5.pet.execution.Job;
import eu.k5.pet.execution.Marking;
import eu.k5.pet.execution.Repository;
import eu.k5.pet.execution.TaskScheduler;
import eu.k5.pet.workflow.Action;
import eu.k5.pet.workflow.ActionContext;
import eu.k5.pet.workflow.Place;
import eu.k5.pet.workflow.TokenType;
import eu.k5.pet.workflow.Transition;
import eu.k5.pet.workflow.TrueGuard;
import eu.k5.pet.workflow.Workflow;

public class TimeoutTest {

	private Repository repository;

	private Engine engine;

	private FixedClock clock;

	private TaskScheduler scheduler;

	private TestScheduleExecutorService executor;

	@Before
	public void init() {
		repository = new Repository();
		clock = new FixedClock(Instant.ofEpochMilli(0l));

		executor = new TestScheduleExecutorService(clock);
		scheduler = new TaskScheduler(clock, executor);

		engine = Engine.builder().withClock(clock).withScheduler(scheduler).withRepository(repository)
				.withWorkflow(getWorkflow()).build();
	}

	@Test
	public void test() throws InterruptedException {
		String id = engine.create(new Job("bk", "init"));
		{
			executor.runAll();

			List<Marking> event = engine.getMarkings(id, TestTokens.EVENT);
			Assert.assertEquals(1, event.size());
			Assert.assertEquals("set", event.get(0).getPlace().getName());
		}

		{
			System.out.println("Increase clock");
			clock.setInstant(Instant.ofEpochMilli(99l));
			executor.runAll();

			List<Marking> event = engine.getMarkings(id, TestTokens.EVENT);
			Assert.assertEquals(1, event.size());
			Assert.assertEquals("set", event.get(0).getPlace().getName());
		}
		{
			System.out.println("Increase clock");
			clock.setInstant(Instant.ofEpochMilli(100l));

			executor.runAll();

			List<Marking> events = engine.getMarkings(id, TestTokens.EVENT);
			Assert.assertEquals(1, events.size());
			Assert.assertEquals("timeout", events.get(0).getPlace().getName());

		}
		Assert.assertEquals(0, scheduler.openTasks());
	}

	private enum TestTokens implements TokenType {
		INIT, EVENT;

		@Override
		public String initValue() {
			return "";
		}

	}

	private static class InitAction implements Action {

		@Override
		public void execute(ActionContext context) {
			System.out.println("Init");
		}

	}

	private static class SetAction implements Action {

		@Override
		public void execute(ActionContext context) {

		}

	}

	private Workflow getWorkflow() {
		Transition transition = new Transition(new TrueGuard(), new InitAction());
		transition.getInput().add(new Place("init", TestTokens.INIT));
		transition.getOutput().add(new Place("set", TestTokens.EVENT));

		Transition transition2 = new Transition(new TimeoutGuard(TestTokens.EVENT, TimeUnit.MILLISECONDS, 100),
				new SetAction());
		transition2.getInput().add(new Place("set", TestTokens.EVENT));
		transition2.getOutput().add(new Place("timeout", TestTokens.EVENT));

		Workflow workflow = new Workflow();
		workflow.addTransition(transition, transition2);
		return workflow;
	}
}
