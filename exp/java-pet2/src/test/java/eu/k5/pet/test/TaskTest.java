package eu.k5.pet.test;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import eu.k5.pet.LtValueGuard;
import eu.k5.pet.SignalAction;
import eu.k5.pet.execution.Job;
import eu.k5.pet.execution.Marking;
import eu.k5.pet.execution.Signal;
import eu.k5.pet.guard.TokenColorGuard;
import eu.k5.pet.strategem.NoOpAction;
import eu.k5.pet.strategem.components.TaskNet;
import eu.k5.pet.test.count.AddAction;
import eu.k5.pet.test.count.InitAction;
import eu.k5.pet.test.count.TestTokens;
import eu.k5.pet.workflow.Place;
import eu.k5.pet.workflow.Transition;
import eu.k5.pet.workflow.TrueGuard;
import eu.k5.pet.workflow.Workflow;

public class TaskTest extends AbstractWorkflowTest {

	@Test
	public void test() {
		engine.create(new Job("test", "xx"));
		String id = engine.findByBusinessKey("test");
		List<Marking> markings = engine.getMarkings(id, TestTokens.INIT);

		Assert.assertEquals(1, markings.size());

		runAll();

		List<Marking> charge = engine.getMarkings(id, TestTokens.SIGNAL_CHARGE);
		System.out.println(charge);

		Signal signal = new Signal(charge.get(0).getValue(), "", "add");

		engine.signal(id, "clicksignal", asJson(signal));

		runAll();

		charge = engine.getMarkings(id, TestTokens.SIGNAL_CHARGE);
		System.out.println(charge);

		engine.signal(id, "clicksignal", asJson(signal));
		runAll();

	}

	@Override
	protected Workflow createWorkflow() {
		Transition transition = new Transition(new TrueGuard(), new InitAction());
		transition.getInput().add(new Place("init", TestTokens.INIT));
		transition.getOutput().add(new Place("add", TestTokens.EVENT));
		transition.getOutput().add(new Place("count", TestTokens.COUNT));
		transition.getOutput().add(new Place("chargeSignal", TestTokens.SIGNAL_CHARGE));

		Place clicked = new Place("addSignal", TestTokens.SIGNAL);
		Place waitClick = new Place("waitClick", TestTokens.SIGNAL);

		TaskNet taskNet = new TaskNet("click", waitClick, clicked, "");

		Transition transition2 = new Transition(new LtValueGuard(TestTokens.COUNT, "5"), new AddAction());
		transition2.getInput().add(clicked);
		transition2.getInput().add(new Place("add", TestTokens.EVENT));
		transition2.getInput().add(new Place("count", TestTokens.COUNT));
		transition2.getOutput().add(new Place("add", TestTokens.EVENT));
		transition2.getOutput().add(new Place("count", TestTokens.COUNT));
		transition2.getOutput().add(new Place("chargeSignal", TestTokens.SIGNAL_CHARGE));

		Workflow workflow = new Workflow();
		workflow.addTransition(transition, transition2);
		workflow.addTransition(taskNet.getTransitions());
		return workflow;
	}

}
