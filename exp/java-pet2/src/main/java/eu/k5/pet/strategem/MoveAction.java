package eu.k5.pet.strategem;

import java.io.IOException;
import java.io.StringReader;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.k5.pet.execution.Marking;
import eu.k5.pet.strategem.Strategem.EventColors;
import eu.k5.pet.strategem.control.MoveJob;
import eu.k5.pet.strategem.control.MoveResult;
import eu.k5.pet.strategem.control.Strat;
import eu.k5.pet.strategem.shared.Board;
import eu.k5.pet.workflow.Action;
import eu.k5.pet.workflow.ActionContext;
import eu.k5.pet.workflow.TokenType;
import eu.k5.pet.workflow.VariableValue;

public class MoveAction implements Action {
	private ObjectMapper mapper = new ObjectMapper();

	@Override
	public void execute(ActionContext context) {
		System.out.println("move");
		VariableValue boardVar = context.getVariable("board");
		VariableValue battleVar = context.getVariable("battle");
		try {
			Board board = mapper.readValue(new StringReader(boardVar.getValue()), Board.class);

			Marking moveMarking = context.getInputPlace(Tokens.MOVE);

			Move move = mapper.readValue(new StringReader(moveMarking.getValue()), Move.class);

			MoveJob job = new MoveJob();

			job.setCurrent(context.getVariable("current").getValue());
			job.setOpponent(context.getVariable("opp").getValue());
			job.setOwner(context.getVariable("owner").getValue());
			job.setSource(move.getSource());
			job.setMovement(move.getDirection());
			job.setBoard(board);

			Strat.move(job);

			MoveResult result = job.getMoveResult();

			context.updateVariable(boardVar, mapper.writeValueAsString(job.getBoard()));

			context.updateVariable(battleVar, mapper.writeValueAsString(job.getBattle()));

			EventColors color = null;
			switch (result) {
			case DECIDE_BOTH:
				System.out.println("Move to decide both");
				color = EventColors.DECIDE_BOTH;
				break;
			case DECIDE_CURRENT:
				color = EventColors.DECIDE_CURRENT;
				break;
			case NEXT:
				color = EventColors.NEXT;
				break;
			case END:
				color = EventColors.WIN;
				break;
			default:
				System.out.println("move result " + result);

				break;
			}
			context.setOutput(Tokens.EVENT, color.name());
			int x = 0;

			x++;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
