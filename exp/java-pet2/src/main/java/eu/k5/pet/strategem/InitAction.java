package eu.k5.pet.strategem;

import eu.k5.pet.execution.Marking;
import eu.k5.pet.strategem.Strategem.EventColors;
import eu.k5.pet.strategem.control.Strat;
import eu.k5.pet.strategem.shared.Battle;
import eu.k5.pet.strategem.shared.Board;
import eu.k5.pet.strategem.shared.Player;
import eu.k5.pet.workflow.Action;
import eu.k5.pet.workflow.ActionContext;

public class InitAction implements Action {

	@Override
	public void execute(ActionContext context) {
		System.out.println("init");
		Marking input = context.getInputPlace(Tokens.INIT);
		String value = input.getValue();

		System.out.println(value);

		StrategemStart start = readJson(value, StrategemStart.class);

		Board board = Strat.initBoard(start.getLayout());

		context.setVariable("owner", start.getOwner());
		context.setVariable("board", asJson(board));
		context.setVariable("current", Player.OWNER.name());
		context.setVariable("battle", asJson(new Battle(null)));

		context.setOutput(Tokens.EVENT, EventColors.OK.name());

	}

}
