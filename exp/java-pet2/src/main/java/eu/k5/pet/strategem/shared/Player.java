package eu.k5.pet.strategem.shared;

public enum Player {
	OWNER, OPPONENT, BATTLE;
}
