package eu.k5.pet.strategem;

import java.util.List;

import eu.k5.pet.execution.Marking;
import eu.k5.pet.workflow.Action;
import eu.k5.pet.workflow.ActionContext;

public class NoOpAction implements Action {

	@Override
	public void execute(ActionContext context) {
		List<Marking> input = context.getInput();

		for (Marking i : input) {
			if (context.hasOutput(i.getPlace().getType())) {
				context.setOutput(i.getPlace().getType(), i.getValue());
			}

		}

		// Marking event = context.getInputPlace(Tokens.EVENT);
		// context.setOutput(Tokens.EVENT, event.getValue());
	}

}
