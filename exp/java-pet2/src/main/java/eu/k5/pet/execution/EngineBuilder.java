package eu.k5.pet.execution;

import java.time.Clock;
import java.util.Objects;

import javax.xml.ws.RespectBinding;

import com.fasterxml.jackson.databind.util.ObjectBuffer;

import eu.k5.pet.workflow.Workflow;

public class EngineBuilder {

	private Clock clock = Clock.systemUTC();

	private Repository repository;

	private TaskScheduler scheduler;

	private Workflow workflow;

	public Clock getClock() {
		return clock;
	}

	public EngineBuilder withClock(Clock clock) {
		this.clock = clock;
		return this;
	}

	public EngineBuilder withRepository(Repository repository) {
		this.repository = repository;
		return this;
	}

	public EngineBuilder withScheduler(TaskScheduler scheduler) {
		this.scheduler = scheduler;
		return this;
	}

	public EngineBuilder withWorkflow(Workflow workflow) {
		this.workflow = workflow;
		return this;
	}

	public Engine build() {
		Objects.requireNonNull(clock);
		Objects.requireNonNull(repository);
		Objects.requireNonNull(scheduler);
		Objects.requireNonNull(workflow);
		return new Engine(clock, scheduler, repository, workflow);
	}

}
