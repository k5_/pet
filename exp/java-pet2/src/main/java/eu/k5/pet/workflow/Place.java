package eu.k5.pet.workflow;

public class Place {
	private String name;
	private TokenType type;

	public Place(String name, TokenType type) {
		this.name = name;
		this.type = type;
	}

	public TokenType getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "Place [name=" + name + ", type=" + type + "]";
	}

}
