package eu.k5.pet.strategem.shared;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Board implements Serializable {
	private static final long serialVersionUID = 1L;

	private final int rows;
	private final int columns;
	private final Piece[][] tokens;

	private Location lastBattle;

	@JsonCreator
	public Board(@JsonProperty("rows") int rows, @JsonProperty("columns") int columns,
			@JsonProperty("tokens") Piece[][] tokens) {
		this.rows = rows;
		this.columns = columns;
		this.tokens = tokens;
		lastBattle = lastBattle;
	}

	public Piece removeToken(Location location) {
		if (!isValidLocation(location)) {
			throw new IllegalArgumentException();
		}

		Piece token = tokens[location.getRow()][location.getColumn()];
		tokens[location.getRow()][location.getColumn()] = null;
		return token;
	}

	public int getRows() {
		return rows;
	}

	public int getColumns() {
		return columns;
	}

	public Piece[][] getTokens() {
		return tokens;
	}

	public boolean isValid(int row, int column) {
		return row >= 0 && row < rows && column >= 0 && column < columns;
	}

	public boolean isValidLocation(Location location) {
		return isValid(location.getRow(), location.getColumn());
	}

	public Player getOwner(int row, int column) {
		Piece token = getTokenAt(row, column);
		if (token == null) {
			return null;
		} else {
			return token.getOwner();
		}
	}

	public Piece getTokenAt(Location location) {
		return getTokenAt(location.getRow(), location.getColumn());
	}

	public Piece getTokenAt(int row, int column) {
		return tokens[row][column];
	}

	public void addToken(Location location, PieceType type, Player player) {
		Piece token = new Piece(location, type, player);
		tokens[location.getRow()][location.getColumn()] = token;
	}

	public void setToken(Piece token) {
		tokens[token.getLocation().getRow()][token.getLocation().getColumn()] = token;
	}

	public Location getLastBattle() {
		return lastBattle;
	}

	public boolean isLastBattle(int row, int column) {
		return new Location(row, column).equals(lastBattle);
	}

	public void setLastBattle(Location battle) {
		lastBattle = battle;
	}

}
