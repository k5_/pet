package eu.k5.pet.strategem.control;

import eu.k5.pet.strategem.shared.Battle;
import eu.k5.pet.strategem.shared.Board;
import eu.k5.pet.strategem.shared.Direction;
import eu.k5.pet.strategem.shared.Location;
import eu.k5.pet.strategem.shared.PieceType;

public class MoveJob {

	private Board board;
	private Location tempLocation;
	private Direction tempDirection;

	private String owner;
	private String opponent;
	private String current;

	private MoveResult moveResult;

	private PieceType decideCurrent;
	private PieceType decideOther;

	private PieceType lastWin;

	public Board getBoard() {
		return board;
	}

	public void setBoard(Board board) {
		this.board = board;
	}

	public Location getSource() {
		return tempLocation;
	}

	public void setSource(Location source) {
		tempLocation = source;
	}

	public Direction getMovement() {
		return tempDirection;
	}

	public void setMovement(Direction movement) {
		tempDirection = movement;
	}

	public MoveResult getMoveResult() {
		return moveResult;
	}

	public void setMoveResult(MoveResult moveResult) {
		this.moveResult = moveResult;
	}

	public PieceType getDecideCurrent() {
		return decideCurrent;
	}

	public void setDecideCurrent(PieceType decideCurrent) {
		this.battle.setCurrent(decideCurrent);
	}

	public void setDecideOther(PieceType decideOther) {
		this.battle.setOther(decideOther);
	}

	@Override
	public String toString() {
		return "MoveJob [board=" + board + ", tempLocation=" + tempLocation + ", tempDirection=" + tempDirection
				+ ", owner=" + owner + ", opponent=" + opponent + ", current=" + current + ", moveResult=" + moveResult
				+ ", decideCurrent=" + decideCurrent + ", decideOther=" + decideOther + ", lastWin=" + lastWin
				+ ", battle=" + battle + "]";
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getOpponent() {
		return opponent;
	}

	public void setOpponent(String opponent) {
		this.opponent = opponent;
	}

	public String getCurrent() {
		return current;
	}

	public void setCurrent(String current) {
		this.current = current;
	}

	private Battle battle;

	public Battle getBattle() {
		return battle;
	}

	public void setBattleLocation(Location location) {
		battle = new Battle(location);
	}

	public PieceType getLastWin() {
		return lastWin;
	}

	public void setLastWin(PieceType lastWin) {
		this.lastWin = lastWin;
	}

}
