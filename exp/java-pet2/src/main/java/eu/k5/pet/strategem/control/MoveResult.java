package eu.k5.pet.strategem.control;

public enum MoveResult {
	NEXT("next"), END("end"), DECIDE_CURRENT("decideCurrent"), DECIDE_OTHER("decideOther"), DECIDE_BOTH("decideBoth");

	private final String value;

	private MoveResult(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public static MoveResult byValue(String value) {
		for (MoveResult result : values()) {
			if (result.getValue().equals(value)) {
				return result;
			}
		}
		return null;
	}

}
