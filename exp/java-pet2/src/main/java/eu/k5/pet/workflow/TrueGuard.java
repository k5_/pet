package eu.k5.pet.workflow;

public class TrueGuard implements Guard {

	@Override
	public boolean canPass(GuardContext execution) {
		return true;
	}

}
