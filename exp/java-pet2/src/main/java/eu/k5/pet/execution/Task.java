package eu.k5.pet.execution;

import java.time.Instant;

public class Task implements Comparable<Task> {

	private final Instant after;
	private final Runnable runnable;
	private final String executionId;

	public Task(Instant after, String executionId, Runnable runnable) {
		this.after = after;
		this.executionId = executionId;
		this.runnable = runnable;
	}

	public Instant getAfter() {
		return after;
	}

	public String getExecutionId() {
		return executionId;
	}

	public Runnable getRunnable() {
		return runnable;
	}

	@Override
	public int compareTo(Task other) {
		int byInstant = other.after.compareTo(after);
		if (byInstant != 0) {
			return byInstant;
		}
		int byId = other.executionId.compareTo(executionId);
		return byId;
	}
}
