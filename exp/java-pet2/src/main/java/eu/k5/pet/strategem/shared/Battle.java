package eu.k5.pet.strategem.shared;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Battle {

	private final Location location;
	private PieceType current;
	private PieceType other;

	@JsonCreator
	public Battle(@JsonProperty("location") Location location) {
		this.location = location;
	}

	public Location getLocation() {
		return location;
	}

	public PieceType getCurrent() {
		return current;
	}

	public PieceType getOther() {
		return other;
	}

	public void setCurrent(PieceType current) {
		this.current = current;
	}

	public void setOther(PieceType other) {
		this.other = other;
	}
}
