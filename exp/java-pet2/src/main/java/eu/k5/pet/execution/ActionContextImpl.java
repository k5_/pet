package eu.k5.pet.execution;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import eu.k5.pet.workflow.ActionContext;
import eu.k5.pet.workflow.TokenType;
import eu.k5.pet.workflow.VariableValue;

public class ActionContextImpl implements ActionContext {
	private List<Marking> input;

	private Map<String, Marking> output;

	private Execution execution;

	private Repository repository;

	public ActionContextImpl(Repository repository, Execution execution, List<Marking> input,
			Map<String, Marking> output) {
		this.repository = repository;
		this.execution = execution;
		this.input = input;
		this.output = output;
	}

	@Override
	public String getExecutionId() {
		return execution.getId();
	}

	public List<Marking> getInput() {
		return input;
	}

	@Override
	public void setOutput(String place, String tokenName, String value) {

		Marking marking = output.get(place);

		output.replace(place, marking.withValue(tokenName, value));
	}

	@Override
	public Marking getInputPlace(TokenType name) {
		List<Marking> found = new ArrayList<>();
		for (Marking marking : getInput()) {
			if (name.equals(marking.getPlace().getType())) {
				found.add(marking);
			}
		}
		return found.get(0);
	}

	@Override
	public void setOutput(TokenType event, String value) {

		for (Marking marking : output.values()) {
			if (event.equals(marking.getPlace().getType())) {
				Marking newMarking = marking.withValue(marking.getName(), value);
				output.replace(marking.getPlace().getName(), newMarking);
			}
		}

	}

	@Override
	public void setVariable(String variable, String value) {

		repository.createVariable(execution.getId(), variable, value);
	}

	@Override
	public VariableValue getVariable(String variableName) {
		return repository.getVariable(execution.getId(), variableName);
	}

	@Override
	public void updateVariable(VariableValue variable, String newValue) {

		repository.updateVariable(execution.getId(), variable, newValue);

	}

	@Override
	public boolean hasOutput(TokenType type) {
		for (Marking m : output.values()) {
			if (m.getPlace().getType().equals(type)) {
				return true;
			}
		}
		return false;
	}

}
