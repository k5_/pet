package eu.k5.pet.strategem.shared;

public enum Layout {
	_4x3(4, 3), _4x4(4, 4), _5x5(5, 5), _5x3(5, 3), _7x7(7, 7);

	private final int rows;
	private final int columns;

	private Layout(int rows, int columns) {
		this.rows = rows;
		this.columns = columns;
	}

	public int getRows() {
		return rows;
	}

	public int getColumns() {
		return columns;
	}

	public String getTitle() {
		return rows + "x" + columns;
	}
}
