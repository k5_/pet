package eu.k5.pet.workflow;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Workflow {

	private Map<String, Place> places = new HashMap<>();

	private List<Transition> transitions = new ArrayList<>();

	public List<Transition> getTransitions() {
		return Collections.unmodifiableList(transitions);
	}

	public void addTransition(Transition... newTransitions) {
		addTransition(Arrays.asList(newTransitions));
	}

	public void addTransition(List<Transition> newTransitions) {

		for (Transition transition : newTransitions) {
			for (Place p : transition.getInput()) {
				addPlace(p);
			}
			for (Place p : transition.getOutput()) {
				addPlace(p);
			}
			transitions.add(transition);
		}
	}

	private void addPlace(Place place) {
		Place existing = places.get(place.getName());
		if (existing == null) {
			places.put(place.getName(), place);
		} else {
			if (!existing.getType().equals(place.getType())) {
				throw new IllegalStateException("Place incompatible");
			}
		}
	}

	public Place getPlace(String name) {
		return places.get(name);
	}

}
