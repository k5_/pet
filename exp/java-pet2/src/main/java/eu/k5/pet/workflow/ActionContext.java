package eu.k5.pet.workflow;

import java.util.List;

import eu.k5.pet.execution.Marking;

public interface ActionContext {

	List<Marking> getInput();

	void setOutput(String place, String tokenName, String value);

	String getExecutionId();

	Marking getInputPlace(TokenType type);

	void setOutput(TokenType event, String value);

	void setVariable(String variable, String value);

	VariableValue getVariable(String string);

	void updateVariable(VariableValue variable, String newValue);

	boolean hasOutput(TokenType type);

}
