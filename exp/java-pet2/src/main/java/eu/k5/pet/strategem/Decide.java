package eu.k5.pet.strategem;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import eu.k5.pet.strategem.shared.PieceType;

public class Decide {

	private final PieceType pieceType;
	private final boolean current;

	@JsonCreator
	public Decide(@JsonProperty("pieceType") PieceType pieceType, @JsonProperty("current") boolean current) {
		this.pieceType = pieceType;
		this.current = current;
	}

	public boolean isCurrent() {
		return current;
	}

	public PieceType getPieceType() {
		return pieceType;
	}
}
