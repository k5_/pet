package eu.k5.pet.execution;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Signal {
	private final String key;
	private final String value;
	private final String owner;

	@JsonCreator
	public Signal(@JsonProperty("key") String key, @JsonProperty("owner") String owner, @JsonProperty("value") String value) {
		this.key = key;
		this.owner = owner;
		this.value = value;
	}

	public String getOwner() {
		return owner;
	}
	
	public String getKey() {
		return key;
	}

	public String getValue() {
		return value;
	}

}
