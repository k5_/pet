package eu.k5.pet.execution;

import java.time.Instant;
import java.time.LocalDateTime;

import eu.k5.pet.workflow.Place;

public class Marking {

	private final Place place;

	private final String name;

	private final String value;

	private final Instant created;

	public Marking(Place place, String name, String value, Instant created) {
		this.place = place;
		this.name = name;
		this.value = value;
		this.created = created;
	}

	public String getName() {
		return name;
	}

	public Place getPlace() {
		return place;
	}

	public String getValue() {
		return value;
	}

	public Instant getCreated() {
		return created;
	}

	public Marking withValue(String tokenName, String newValue) {
		return new Marking(place, tokenName, newValue, created);
	}

	@Override
	public String toString() {
		return "Marking [place=" + place + ", name=" + name + ", value=" + value + "]";
	}

	
}

