package eu.k5.pet.strategem;

import eu.k5.pet.workflow.Action;
import eu.k5.pet.workflow.ActionContext;
import eu.k5.pet.workflow.VariableValue;

public class NextAction implements Action {

	@Override
	public void execute(ActionContext context) {
		System.out.println("next");

		VariableValue current = context.getVariable("current");
		VariableValue other = context.getVariable("other");

		context.updateVariable(current, other.getValue());
		context.updateVariable(other, current.getValue());
	}

}
