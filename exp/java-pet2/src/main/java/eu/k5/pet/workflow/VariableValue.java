package eu.k5.pet.workflow;

public class VariableValue {

	private final String name;
	private final long version;
	private final String value;

	public VariableValue(String name, long version, String value) {
		this.name = name;
		this.version = version;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public long getVersion() {
		return version;
	}

	public String getValue() {
		return value;
	}

}
