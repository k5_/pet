package eu.k5.pet.strategem.control;

import eu.k5.pet.strategem.shared.Board;
import eu.k5.pet.strategem.shared.Layout;
import eu.k5.pet.strategem.shared.Location;
import eu.k5.pet.strategem.shared.Piece;
import eu.k5.pet.strategem.shared.PieceType;
import eu.k5.pet.strategem.shared.Player;

public class Strat {

	public static void move(MoveJob job) {
		Player current = getCurrent(job);

		Location battle = job.getSource().move(job.getMovement());
		if (!job.getBoard().isValidLocation(battle)) {
			throw new IllegalArgumentException("illegal move");
		}

	
		job.setBattleLocation(battle);

		
		Piece source = job.getBoard().getTokenAt(job.getSource());
		if (source == null) {
			throw new RuntimeException("Token not found");
		}
		if (!source.getOwner().equals(current)) {
			throw new RuntimeException("Not owning token");
		}

		job.getBoard().removeToken(job.getSource());

		Piece targetToken = job.getBoard().removeToken(battle);
		if (targetToken == null) {
			job.getBoard().addToken(battle, source.getType(), source.getOwner());
			job.setMoveResult(MoveResult.NEXT);
		} else if (targetToken.getOwner().equals(source.getOwner())) {
			throw new IllegalArgumentException("cant attack own figures");
		} else {
			job.getBoard().setLastBattle(battle);
			if (targetToken.getType().equals(PieceType.FLAG)) {
				job.getBoard().setToken(new Piece(battle, PieceType.CAPTURED_FLAG, current));
				job.setMoveResult(MoveResult.END);
			} else if (source.getType().equals(PieceType.UNSPECIFIED)
					&& targetToken.getType().equals(PieceType.UNSPECIFIED)) {
				job.setDecideCurrent(PieceType.UNSPECIFIED);
				job.setDecideOther(PieceType.UNSPECIFIED);
				job.getBoard().setToken(new Piece(battle, PieceType.BATTLE, Player.BATTLE));
				job.setMoveResult(MoveResult.DECIDE_BOTH);
			} else if (source.getType().equals(PieceType.UNSPECIFIED)) {
				job.setDecideCurrent(PieceType.UNSPECIFIED);
				job.setDecideOther(targetToken.getType());
				job.getBoard().setToken(new Piece(battle, PieceType.BATTLE, Player.BATTLE));

				job.setMoveResult(MoveResult.DECIDE_CURRENT);
			} else if (targetToken.getType().equals(PieceType.UNSPECIFIED)) {
				job.setDecideCurrent(source.getType());
				job.setDecideOther(PieceType.UNSPECIFIED);
				job.getBoard().setToken(new Piece(battle, PieceType.BATTLE, Player.BATTLE));

				job.setMoveResult(MoveResult.DECIDE_OTHER);
			} else if (targetToken.getType().equals(source.getType())) {
				job.setDecideCurrent(source.getType());
				job.setDecideOther(PieceType.UNSPECIFIED);
				job.getBoard().setToken(new Piece(battle, PieceType.BATTLE, Player.BATTLE));

				job.setMoveResult(MoveResult.DECIDE_BOTH);
			} else if (source.getType().win(targetToken.getType())) {
				job.getBoard().addToken(battle, source.getType(), source.getOwner());
				job.setMoveResult(MoveResult.NEXT);
			} else if (targetToken.getType().win(source.getType())) {
				job.setMoveResult(MoveResult.NEXT);
				job.getBoard().setToken(targetToken);
			} else {
				throw new IllegalArgumentException("Invalid state");
			}
		}

	}

	private static Player getCurrent(MoveJob job) {
	
		if (job.getCurrent().equals(		job.getOwner())){
			return Player.OWNER;
		} else {
			return Player.OPPONENT;
		}
	}
	
	

	public static Board initBoard(Layout layout) {
		Piece[][] tokens = new Piece[layout.getRows()][layout.getColumns()];

		Board board = new Board(layout.getRows(), layout.getColumns(), tokens);
		for (int i = 0; i < layout.getColumns(); i++) {
			board.addToken(new Location(0, i), PieceType.UNSPECIFIED, Player.OWNER);
			board.addToken(new Location(1, i), PieceType.UNSPECIFIED, Player.OWNER);

			board.addToken(new Location(layout.getRows() - 2, i), PieceType.UNSPECIFIED, Player.OPPONENT);
			board.addToken(new Location(layout.getRows() - 1, i), PieceType.UNSPECIFIED, Player.OPPONENT);

		}
		return board;
	}
}
