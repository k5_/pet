package eu.k5.pet.guard;

import eu.k5.pet.workflow.Guard;
import eu.k5.pet.workflow.GuardContext;

public class TokenValueGuard implements Guard {
	private String tokenName;

	private String value;

	public TokenValueGuard(String tokenName, String value) {
		this.tokenName = tokenName;
		this.value = value;
	}

	@Override
	public boolean canPass(GuardContext context) {
		return value.equals(context.getMarkings().get(tokenName).getValue());
	}

}
