package eu.k5.pet.strategem;

import java.io.IOException;
import java.io.StringReader;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.k5.pet.execution.Marking;
import eu.k5.pet.execution.Token;
import eu.k5.pet.strategem.shared.Board;
import eu.k5.pet.strategem.shared.Location;
import eu.k5.pet.strategem.shared.Piece;
import eu.k5.pet.strategem.shared.PieceType;
import eu.k5.pet.strategem.shared.Player;
import eu.k5.pet.workflow.Action;
import eu.k5.pet.workflow.ActionContext;
import eu.k5.pet.workflow.TokenType;
import eu.k5.pet.workflow.VariableValue;

public class SelectFlagAction implements Action {

	private final Player player;

	public SelectFlagAction(Player player) {
		this.player = player;
	}

	private ObjectMapper objectMapper = new ObjectMapper();

	@Override
	public void execute(ActionContext context) {
		System.out.println("Select flag" + player);

		Marking marking = context.getInputPlace(Tokens.SELECT);

		String value = marking.getValue();

		try {
			Location loc = objectMapper.readValue(value, Location.class);

			VariableValue variable = context.getVariable("board");
			Board board = objectMapper.readValue(new StringReader(variable.getValue()), Board.class);

			Piece token = board.getTokenAt(loc);

			if (player.equals(token.getOwner())) {

				board.addToken(loc, PieceType.FLAG, player);

				String asJson = objectMapper.writeValueAsString(board);

				context.updateVariable(variable, asJson);

				return;
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("select");
	}

}
