package eu.k5.pet.strategem.actions;

import eu.k5.pet.execution.Marking;
import eu.k5.pet.strategem.Join;
import eu.k5.pet.strategem.Tokens;
import eu.k5.pet.workflow.Action;
import eu.k5.pet.workflow.ActionContext;

public class JoinAction implements Action {

	@Override
	public void execute(ActionContext context) {

		Marking input = context.getInputPlace(Tokens.JOIN);

		Join join= readJson(input.getValue(), Join.class);
		System.out.println("join" + input.getValue());
		
		context.setVariable("opp", join.getOpponent());

	}

}
