package eu.k5.pet.strategem.components;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.k5.pet.execution.Engine;
import eu.k5.pet.execution.Marking;
import eu.k5.pet.execution.Signal;
import eu.k5.pet.execution.TaskSignal;
import eu.k5.pet.guard.TokenColorGuard;
import eu.k5.pet.strategem.NoOpAction;
import eu.k5.pet.test.NegateGuard;
import eu.k5.pet.test.count.TestTokens;
import eu.k5.pet.workflow.Action;
import eu.k5.pet.workflow.ActionContext;
import eu.k5.pet.workflow.Place;
import eu.k5.pet.workflow.TokenType;
import eu.k5.pet.workflow.Transition;
import eu.k5.pet.workflow.TrueGuard;
import eu.k5.pet.workflow.VariableValue;

public class TaskNet {

	private String prefix;

	private Place input;

	private Place output;

	private String ownerVar;

	public TaskNet(String prefix, Place input, Place output, String ownerVar) {
		this.prefix = prefix;
		this.input = input;
		this.output = output;
		this.ownerVar = ownerVar;
	}

	public List<Transition> getTransitions() {
		List<Transition> transitions = new ArrayList<>();

		Transition moveToCharge = new Transition(new TrueGuard(), new ChargeAction(ownerVar));
		moveToCharge.getInput().add(input);
		moveToCharge.getOutput().add(new Place(prefix + "chargeSignal", TestTokens.SIGNAL_CHARGE));

		Transition tSignal = new Transition(new TrueGuard(),
				new SignalAction(TestTokens.SIGNAL_CHARGE, TestTokens.SIGNAL));
		tSignal.getInput().add(new Place(prefix + "chargeSignal", TestTokens.SIGNAL_CHARGE));
		tSignal.getInput().add(new Place(prefix + "signal", TestTokens.SIGNAL));
		tSignal.getOutput().add(new Place(prefix + "matched", TestTokens.EVENT));
		tSignal.getOutput().add(new Place(prefix + "matchedSignal", TestTokens.SIGNAL));

		Transition tRecharge = new Transition(new NegateGuard(new TokenColorGuard(TestTokens.EVENT, "OK")),
				new NoOpAction());

		tRecharge.getInput().add(new Place(prefix + "matched", TestTokens.EVENT));
		tRecharge.getInput().add(new Place(prefix + "matchedSignal", TestTokens.SIGNAL));
		tRecharge.getOutput().add(new Place(prefix + "chargeSignal", TestTokens.SIGNAL_CHARGE));

		Transition tForward = new Transition(new TokenColorGuard(TestTokens.EVENT, "OK"), new CopyToOut(output));
		tForward.getInput().add(new Place(prefix + "matched", TestTokens.EVENT));
		tForward.getInput().add(new Place(prefix + "matchedSignal", TestTokens.SIGNAL));
		tForward.getOutput().add(output);

		transitions.add(moveToCharge);
		transitions.add(tSignal);
		transitions.add(tRecharge);
		transitions.add(tForward);

		return transitions;
	}

	static class SignalAction implements Action {

		private final TokenType charge;
		private final TokenType signal;

		public SignalAction(TokenType charge, TokenType signal) {
			this.charge = charge;
			this.signal = signal;
		}

		@Override
		public void execute(ActionContext context) {
			Marking chargePlace = context.getInputPlace(charge);
			Marking signalPlace = context.getInputPlace(signal);

			TaskStore key = readJson(chargePlace.getValue(), TaskStore.class);

			Signal signal = readJson(signalPlace.getValue(), Signal.class);

			if (signal.getKey().equals(key.getKey())) {

				if (!signal.getOwner().equals(key.getOwner())) {
					System.out.println("Owner didnt match");
				} else {

					System.out.println("Signal matched");
					context.setOutput(TestTokens.EVENT, "OK");
					context.setOutput(TestTokens.SIGNAL, signal.getValue());
				}
			} else {
				System.out.println("Signal no matched");

				context.setOutput(TestTokens.EVENT, "NOK");
			}

		}

	}

	public static Task findTask(Engine engine, String id, String prefix, String owner) {

		List<Marking> markings = engine.getMarkings(id, TestTokens.SIGNAL_CHARGE);

		for (Marking m : markings) {
			if (m.getPlace().getName().startsWith(prefix)) {
				TaskStore store = readValue(m.getValue(), TaskStore.class);
				if (store.getOwner().equals(owner)) {
					return new Task(store.getKey(), prefix + "signal");
				}
			}
		}
		return null;
	}

	// public static Task findChargeKey(Engine engine, String id, String prefix)
	// {
	// List<Marking> markings = engine.getMarkings(id,
	// TestTokens.SIGNAL_CHARGE);
	//
	// for (Marking m : markings) {
	// if (m.getPlace().getName().startsWith(prefix)) {
	// TaskStore store = readValue(m.getValue(), TaskStore.class);
	// return new Task(store.getKey(), prefix + "signal");
	// }
	// }
	// return null;
	// }

	public static class Task {
		private final String key;
		private final String signalPlace;

		public Task(String key, String signalPlace) {
			this.key = key;
			this.signalPlace = signalPlace;
		}

		public String getKey() {
			return key;
		}

		public TaskSignal signal(String owner, Object value) {
			return new TaskSignal(key, signalPlace, owner, value);
		}
	}

	private static class TaskStore {
		private final String key;
		private final String owner;

		public TaskStore(@JsonProperty("key") String key, @JsonProperty("owner") String owner) {
			this.key = key;
			this.owner = owner;
		}

		public String getKey() {
			return key;
		}

		public String getOwner() {
			return owner;
		}

	}

	private static class CopyToOut implements Action {
		private Place output;

		public CopyToOut(Place output) {
			this.output = output;
		}

		@Override
		public void execute(ActionContext context) {
			Marking signal = context.getInputPlace(TestTokens.SIGNAL);
			context.setOutput(output.getName(), output.getType().toString(), signal.getValue());
		}

	}

	private static <T> T readValue(String json, Class<T> type) {
		try {
			return new ObjectMapper().readValue(new StringReader(json), type);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private class ChargeAction implements Action {
		private final String ownerVar;

		public ChargeAction(String ownerVar) {
			this.ownerVar = ownerVar;
		}

		@Override
		public void execute(ActionContext context) {
			String ownerValue;
			if (ownerVar.isEmpty()) {
				ownerValue = "";
			} else {
				VariableValue owner = context.getVariable(ownerVar);
				if (owner == null) {
					System.out.println(ownerVar + " is nullS");
				}
				ownerValue = owner.getValue();
			}

			TaskStore store = new TaskStore(UUID.randomUUID().toString(), ownerValue);

			context.setOutput(TestTokens.SIGNAL_CHARGE, asJson(store));
		}

	}
}
