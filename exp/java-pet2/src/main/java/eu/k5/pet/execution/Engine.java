package eu.k5.pet.execution;

import java.time.Clock;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.k5.pet.workflow.Place;
import eu.k5.pet.workflow.TokenType;
import eu.k5.pet.workflow.Transition;
import eu.k5.pet.workflow.Workflow;

public class Engine {
	private static final String INIT_PLACE = "init";

	private final Repository repository;

	private final TaskScheduler scheduler;

	private final Workflow workflow;

	private final Clock clock;

	public Engine(Clock clock, TaskScheduler scheduler, Repository repository, Workflow workflow) {
		this.clock = clock;
		this.scheduler = scheduler;
		this.repository = repository;
		this.workflow = workflow;
	}

	public String create(Job job) {
		Execution execution = new Execution(null, job.getBusinessKey());
		Place place = workflow.getPlace(INIT_PLACE);
		Marking marking = new Marking(place, "init", job.getValue(), clock.instant());

		String id = repository.insert(execution, marking);

		scheduler.scheduleNow(id, () -> exec(id));

		return id;
	}

	void exec(String id) {

		Execution execution = repository.getById(id);

		CanFire couldFire = null;
		while ((couldFire = exec(execution)).canFire) {
			execution = repository.getById(id);
		}
		if (couldFire.retest != null) {
			scheduler.schedule(id, () -> exec(id), couldFire.retest);
		}
	}

	private CanFire exec(Execution execution) {
		TreeSet<Instant> retests = new TreeSet<>();

		List<Transition> canFire = new ArrayList<>();
		for (Transition transition : workflow.getTransitions()) {
			CanFire can = canFire(transition, execution);
			if (can.canFire) {
				canFire.add(transition);
			} else if (can.retest != null) {
				retests.add(can.retest);
			}
		}

		if (canFire.isEmpty()) {
			if (retests.isEmpty()) {
				return new CanFire(false, null);
			} else {
				return new CanFire(false, retests.first());
			}

		}
		if (canFire.size() > 1) {
			// throw new IllegalStateException("Multiple transitions possible");
		}

		fire(canFire.get(0), execution);
		return new CanFire(true, null);
	}

	private void fire(Transition transition, Execution execution) {
		List<Marking> input = new ArrayList<>();
		for (Place place : transition.getInput()) {
			input.add(execution.getMarking(place.getName()));
		}

		repository.moveToTransit(execution.getId(), input);

		Map<String, Marking> output = new HashMap<>();
		for (Place place : transition.getOutput()) {
			String initValue = place.getType().initValue();
			output.put(place.getName(), new Marking(place, place.getName(), initValue, clock.instant()));
		}

		ActionContextImpl context = new ActionContextImpl(repository, execution, input, output);

		transition.getAction().execute(context);

		for (Marking out : output.values()) {
			repository.addMarking(execution.getId(), out);
		}
	}

	private CanFire canFire(Transition transition, Execution execution) {
		Map<String, Marking> markings = new HashMap<>();
		for (Place place : transition.getInput()) {
			Marking marking = execution.getMarking(place.getName());
			if (marking == null) {
				return new CanFire(false, null);
			} else {
				markings.put(marking.getName(), marking);
			}
		}

		GuardContextImpl context = new GuardContextImpl(clock, markings);
		boolean canPass = transition.getGuard().canPass(context);
		if (!canPass) {
			Instant retest = transition.getGuard().retest(context);
			return new CanFire(false, retest);
		}
		return new CanFire(canPass, null);
	}

	static class CanFire {
		private boolean canFire;
		private Instant retest;

		public CanFire(boolean canFire, Instant retest) {
			this.canFire = canFire;
			this.retest = retest;
		}

	}

	public void signal(String executionId, TaskSignal signal) {

		ObjectMapper mapper = new ObjectMapper();

		String sigValue;
		try {
			String val = mapper.writeValueAsString(signal.getValue());
			sigValue = mapper.writeValueAsString(new Signal(signal.getKey(), signal.getOwner(), val));
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}

		signal(executionId, signal.getSignalPlace(), sigValue);
	}

	public void signal(String executionId, String placeName, String value) {
		Place place = workflow.getPlace(placeName);

		if (place == null) {
			throw new RuntimeException("Unknown place " + placeName);
		}
		Marking marking = new Marking(place, placeName, value, clock.instant());
		repository.addMarking(executionId, marking);

		scheduler.scheduleNow(executionId, () -> exec(executionId));
	}


	public List<Marking> getMarkings(String id) {
		Execution exec = repository.getById(id);
		if (exec == null) {
			return Collections.emptyList();
		}
		return exec.getMarkings();		
	}

	public List<Marking> getMarkings(String id, TokenType count) {
		Execution exec = repository.getById(id);
		if (exec == null) {
			return Collections.emptyList();
		}
		List<Marking> markings = new ArrayList<>();
		for (Marking marking : exec.getMarkings()) {
			if (marking.getPlace().getType().equals(count)) {
				markings.add(marking);
			}
		}
		return markings;
	}

	public static EngineBuilder builder() {
		return new EngineBuilder();
	}

	public String findByBusinessKey(String key) {
		return repository.getByBusinessKey(key);
	}

}
