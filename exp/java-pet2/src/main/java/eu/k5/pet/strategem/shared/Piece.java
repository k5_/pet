package eu.k5.pet.strategem.shared;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Piece implements Serializable {
	private static final long serialVersionUID = 1L;

	private final Location location;
	private final PieceType type;
	private final Player owner;

	@JsonCreator
	public Piece(@JsonProperty("location") Location location, @JsonProperty("type") PieceType type,
			@JsonProperty("owner") Player owner) {
		this.location = location;
		this.type = type;
		this.owner = owner;
	}

	public Location getLocation() {
		return location;
	}

	public PieceType getType() {
		return type;
	}

	public Player getOwner() {
		return owner;
	}

	@Override
	public String toString() {
		return "Token [location=" + location + ", type=" + type + ", owner=" + owner + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (location == null ? 0 : location.hashCode());
		result = prime * result + (owner == null ? 0 : owner.hashCode());
		result = prime * result + (type == null ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Piece other = (Piece) obj;
		if (location == null) {
			if (other.location != null) {
				return false;
			}
		} else if (!location.equals(other.location)) {
			return false;
		}
		if (owner != other.owner) {
			return false;
		}
		if (type != other.type) {
			return false;
		}
		return true;
	}

}
