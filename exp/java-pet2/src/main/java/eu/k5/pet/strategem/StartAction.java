package eu.k5.pet.strategem;

import eu.k5.pet.workflow.Action;
import eu.k5.pet.workflow.ActionContext;
import eu.k5.pet.workflow.VariableValue;

public class StartAction implements Action {

	@Override
	public void execute(ActionContext context) {
		System.out.println("Start");

		VariableValue variable = context.getVariable("current");
		context.updateVariable(variable, context.getVariable("owner").getValue());
		context.setVariable("other", context.getVariable("opp").getValue());
	}

}
