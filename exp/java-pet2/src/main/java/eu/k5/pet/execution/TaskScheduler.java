package eu.k5.pet.execution;

import java.time.Clock;
import java.time.Instant;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class TaskScheduler {

	private final Clock clock;
	private final PriorityBlockingQueue<Task> tasks = new PriorityBlockingQueue<>();

	private final ScheduledExecutorService executor;

	public TaskScheduler(Clock clock, ScheduledExecutorService executor) {
		this.clock = clock;
		this.executor = executor;
	}

	public void scheduleNow(String id, Runnable runnable) {
		
		tasks.add(new Task(clock.instant(), id, runnable));
		executor.submit(this::exec);
	}

	public void schedule(String id, Runnable runnable, Instant when) {

		tasks.add(new Task(when, id, runnable));
		executor.schedule(this::exec, when.toEpochMilli() - clock.instant().toEpochMilli(), TimeUnit.MILLISECONDS);
	}

	private void exec() {

		for (;;) {
			Task task = tasks.poll();
			if (task == null) {
				System.out.println("no task found");
				return;
			}
			Instant instant = clock.instant();
			if (instant.isBefore(task.getAfter())) {
				tasks.add(task);
				System.out.println("Task not ready");
				return;
			}

			task.getRunnable().run();
		}
	}
	
	public long openTasks(){
		return tasks.size();
	}

	public void shutdown() {
		// TODO Auto-generated method stub

	}
}
