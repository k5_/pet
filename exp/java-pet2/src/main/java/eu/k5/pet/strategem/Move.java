package eu.k5.pet.strategem;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import eu.k5.pet.strategem.shared.Direction;
import eu.k5.pet.strategem.shared.Location;

public class Move {
	private final Location source;
	private final Direction direction;

	@JsonCreator
	public Move(@JsonProperty("source") Location source, @JsonProperty("direction") Direction direction) {
		this.source = source;
		this.direction = direction;
	}

	public Location getSource() {
		return source;
	}

	public Direction getDirection() {
		return direction;
	}
}
