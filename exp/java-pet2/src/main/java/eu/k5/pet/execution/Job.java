package eu.k5.pet.execution;

public class Job {
	private final String businessKey;

	private final String value;

	public Job(String businessKey, String value) {
		this.businessKey = businessKey;
		this.value = value;
	}

	public String getBusinessKey() {
		return businessKey;
	}

	public String getValue() {
		return value;
	}
}
