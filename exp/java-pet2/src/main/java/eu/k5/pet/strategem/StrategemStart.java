package eu.k5.pet.strategem;

import java.util.Date;

import eu.k5.pet.strategem.shared.Layout;

public class StrategemStart {
	private String game;
	private String owner;
	private String current;

	private String ownerName;
	private String gameName;
	private boolean open;

	private Layout layout = Layout._4x3;
	private int rows;

	private int columns;

	private Date started;

	public Layout getLayout() {
		return layout;
	}

	public void setLayout(Layout layout) {
		this.layout = layout;
	}

	public String getGame() {
		return game;
	}

	public void setGame(String game) {
		this.game = game;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		current = owner;
		this.owner = owner;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public void setGameName(String gameName) {

	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	public Date getStarted() {
		return started;
	}

	public void setStarted(Date started) {
		this.started = started;
	}

	public String getGameName() {
		return gameName;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getColumns() {
		return columns;
	}

	public void setColumns(int columns) {
		this.columns = columns;
	}

}
