package eu.k5.pet.execution;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.k5.pet.workflow.VariableValue;

public class Repository {
	private static final Logger LOGGER = LoggerFactory.getLogger(Repository.class);

	private long id = 0l;

	private final Map<String, Execution> executions = new HashMap<>();
	private final Map<Variable, VariableValue> variables = new HashMap<>();

	private static class Variable {
		private String executionId;
		private String variable;

		public Variable(String executionId, String variable) {
			this.executionId = executionId;
			this.variable = variable;
		}

		public String getExecutionId() {
			return executionId;
		}

		public String getVariable() {
			return variable;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((executionId == null) ? 0 : executionId.hashCode());
			result = prime * result + ((variable == null) ? 0 : variable.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Variable other = (Variable) obj;
			if (executionId == null) {
				if (other.executionId != null)
					return false;
			} else if (!executionId.equals(other.executionId))
				return false;
			if (variable == null) {
				if (other.variable != null)
					return false;
			} else if (!variable.equals(other.variable))
				return false;
			return true;
		}

	}

	public synchronized String insert(Execution execution, Marking marking) {
		String id = newId();
		Execution exec = new Execution(id, execution.getBusinessKey());
		executions.put(id, exec);
		addMarking(id, marking);

		return id;
	}

	private String newId() {
		return "ID_" + (++id);
	}

	public synchronized Execution getById(String executionId) {
		Execution exec = executions.get(executionId);

		return exec.copy();
	}

	public synchronized void addMarking(String executionId, Marking marking) {
		Execution exec = executions.get(executionId);
		exec.addMarking(marking);
	}

	public synchronized void moveToTransit(String executionId, List<Marking> input) {

		Execution exec = executions.get(executionId);

		exec.moveToTransit(input);

	}

	public synchronized void createVariable(String executionId, String variableName, String value) {
		LOGGER.info("create variable {} for execution {}", variableName, executionId);

		Variable variable = new Variable(executionId, variableName);
		VariableValue current = variables.putIfAbsent(variable, new VariableValue(variableName, 0l, value));
		if (current != null) {
			throw new OptimisticLockingException();
		}
	}

	public synchronized VariableValue getVariable(String executionId, String variableName) {
		Variable variable = new Variable(executionId, variableName);

		return variables.get(variable);
	}

	public synchronized void updateVariable(String executionId, VariableValue oldVariable, String newValue) {
		LOGGER.info("Updating variable {} for execution {}", oldVariable.getName(), executionId);

		Variable variable = new Variable(executionId, oldVariable.getName());

		VariableValue current = variables.get(variable);

		if (current == null || current.getVersion() != oldVariable.getVersion()) {
			throw new OptimisticLockingException();
		}

		VariableValue newVariable = new VariableValue(current.getName(), current.getVersion() + 1, newValue);

		variables.put(variable, newVariable);
	}

	public String getByBusinessKey(String key) {
		for (Execution execution : executions.values()) {
			if (key.equals(execution.getBusinessKey())) {
				return execution.getId();
			}
		}
		return null;
	}
}
