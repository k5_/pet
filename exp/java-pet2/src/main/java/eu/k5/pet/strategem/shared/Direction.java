package eu.k5.pet.strategem.shared;

public enum Direction {
	UP, DOWN, LEFT, RIGHT;
}
