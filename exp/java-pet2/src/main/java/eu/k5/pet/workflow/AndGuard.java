package eu.k5.pet.workflow;

import java.util.List;

import eu.k5.pet.execution.Execution;

public class AndGuard implements Guard {

	private final List<Guard> guards;

	public AndGuard(List<Guard> guards) {
		this.guards = guards;
	}

	@Override
	public boolean canPass(GuardContext context) {
		for (Guard guard : guards) {
			if (!guard.canPass(context)) {
				return false;
			}
		}
		return true;
	}

}
