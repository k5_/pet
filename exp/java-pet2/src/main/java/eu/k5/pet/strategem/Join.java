package eu.k5.pet.strategem;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Join {
	private String opponent;

	public Join(@JsonProperty("property") String opponent) {
		this.opponent = opponent;
	}

	public String getOpponent() {
		return opponent;
	}
}
