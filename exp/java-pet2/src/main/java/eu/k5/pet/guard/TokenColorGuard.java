package eu.k5.pet.guard;

import eu.k5.pet.execution.Marking;
import eu.k5.pet.workflow.Guard;
import eu.k5.pet.workflow.GuardContext;
import eu.k5.pet.workflow.TokenType;

public class TokenColorGuard implements Guard {

	private final TokenType type;

	private final String color;

	public TokenColorGuard(TokenType type, String color) {
		this.type = type;
		this.color = color;
	}

	@Override
	public boolean canPass(GuardContext context) {
		Marking marking = context.getMarking(type);
		return marking.getValue() != null && marking.getValue().equals(color);
	}

}
