package eu.k5.pet.execution;

import java.time.Clock;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import eu.k5.pet.workflow.GuardContext;
import eu.k5.pet.workflow.TokenType;

public class GuardContextImpl implements GuardContext {
	private final Map<String, Marking> markings;
	private final Clock clock;

	public GuardContextImpl(Clock clock, Map<String, Marking> markings) {
		this.clock = clock;
		this.markings = markings;
	}

	@Override
	public Map<String, Marking> getMarkings() {
		return markings;
	}

	@Override
	public Marking getMarking(TokenType name) {
		List<Marking> found = new ArrayList<>();
		for (Marking marking : markings.values()) {
			if (name.equals(marking.getPlace().getType())) {
				found.add(marking);
			}
		}
		return found.get(0);
	}

	@Override
	public Clock getClock() {
		return clock;
	}
}
