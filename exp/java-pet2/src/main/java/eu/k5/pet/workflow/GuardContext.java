package eu.k5.pet.workflow;

import java.time.Clock;
import java.util.Map;

import eu.k5.pet.execution.Marking;

public interface GuardContext {

	Map<String, Marking> getMarkings();

	Marking getMarking(TokenType token);

	Clock getClock();

}
