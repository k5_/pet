package eu.k5.pet.strategem;

import java.time.Clock;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.k5.pet.execution.Engine;
import eu.k5.pet.execution.Job;
import eu.k5.pet.execution.Repository;
import eu.k5.pet.execution.TaskScheduler;
import eu.k5.pet.guard.TokenColorGuard;
import eu.k5.pet.strategem.actions.JoinAction;
import eu.k5.pet.strategem.components.TaskNet;
import eu.k5.pet.strategem.shared.Direction;
import eu.k5.pet.strategem.shared.Layout;
import eu.k5.pet.strategem.shared.Location;
import eu.k5.pet.strategem.shared.PieceType;
import eu.k5.pet.strategem.shared.Player;
import eu.k5.pet.workflow.Color;
import eu.k5.pet.workflow.Place;
import eu.k5.pet.workflow.Transition;
import eu.k5.pet.workflow.TrueGuard;
import eu.k5.pet.workflow.Workflow;

public class Strategem {

	enum EventColors implements Color {
		NOP, OK, NOK, DECIDE_BOTH, DECIDE_CURRENT, DECIDE_OTHER, NEXT, WIN, ERR;
	}

	/**
	 * @param args
	 * @throws InterruptedException
	 * @throws JsonProcessingException
	 */
	public static void main(String[] args) throws InterruptedException, JsonProcessingException {

		Workflow workflow = createWorkflow();
		Repository repository = new Repository();
		TaskScheduler scheduler = new TaskScheduler(Clock.systemUTC(), Executors.newScheduledThreadPool(1));
		Engine engine = Engine.builder().withRepository(repository).withWorkflow(workflow).withScheduler(scheduler)
				.build();

		String boardJson = new ObjectMapper().writeValueAsString(Layout._4x3);

		// String boardJson = new ObjectMapper().writeValueAsString(board);
		System.out.println(boardJson);

		String id = engine.create(new Job("a", "_4x3"));

		engine.signal(id, "ownerSelectSignal", asJson(new Location(0, 0)));
		engine.signal(id, "opponentSelectSignal", asJson(new Location(2, 2)));

		engine.signal(id, "moveSignal", asJson(new Move(new Location(1, 0), Direction.DOWN)));

		engine.signal(id, "decideCurrent", asJson(new Decide(PieceType.ROCK,true)));
		engine.signal(id, "decideOther", asJson(new Decide(PieceType.PAPER, false)));

		engine.signal(id, "moveSignal", asJson(new Move(new Location(2, 0), Direction.UP)));

		engine.signal(id, "moveSignal", asJson(new Move(new Location(1, 1), Direction.LEFT)));

		engine.signal(id, "decideCurrent", asJson(new Decide(PieceType.PAPER, true)));

		engine.signal(id, "decideCurrent", asJson(new Decide(PieceType.ROCK, true)));
		engine.signal(id, "decideOther", asJson(new Decide(PieceType.PAPER, false)));
		engine.signal(id, "moveSignal", asJson(new Move(new Location(1, 0), Direction.UP)));

		scheduler.shutdown();
		Thread.sleep(500l);

		System.err.println(repository.getById(id).getMarkings());
		System.out.println(repository.getVariable(id, "board").getValue());
		System.out.println(repository.getVariable(id, "battle").getValue());
	}

	private static String asJson(Object value) {
		try {
			return new ObjectMapper().writeValueAsString(value);
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}

	public static Workflow createWorkflow() {
		Place ownerSelectPlace = new Place("ownerSelect", Tokens.EVENT);
		Place oppJoinPlace = new Place("opponentSelect", Tokens.EVENT);

		Transition init = new Transition(new TrueGuard(), new InitAction());
		init.getInput().add(new Place("init", Tokens.INIT));
		init.getOutput().add(ownerSelectPlace);
		init.getOutput().add(oppJoinPlace);

		Place ownerSelectSignal = new Place("ownerSelectSignal", Tokens.SELECT);
		TaskNet ownerSelectTask = new TaskNet("ownerSelect", ownerSelectPlace, ownerSelectSignal, "owner");

		Transition ownerSelect = new Transition(new TrueGuard(), new SelectFlagAction(Player.OWNER));
		ownerSelect.getInput().add(ownerSelectSignal);
		ownerSelect.getOutput().add(new Place("ownerSelected", Tokens.EVENT));

		Place joinedPlace = new Place("joined", Tokens.JOIN);
		TaskNet joinTask = new TaskNet("join", oppJoinPlace, joinedPlace, "");

		Place oppSelect = new Place("oppSelect", Tokens.SELECT);
		Transition join = new Transition(new TrueGuard(), new JoinAction());
		join.getInput().add(joinedPlace);
		join.getOutput().add(oppSelect);

		Place oppSelected = new Place("oppSelected", Tokens.SELECT);

		TaskNet oppSelectTask = new TaskNet("oppSelect", oppSelect, oppSelected, "opp");

		Transition opponentSelect = new Transition(new TrueGuard(), new SelectFlagAction(Player.OPPONENT));
		opponentSelect.getInput().add(oppSelected);
		opponentSelect.getOutput().add(new Place("opponentSelected", Tokens.EVENT));

		Transition start = new Transition(new TrueGuard(), new StartAction());
		start.getInput().add(new Place("ownerSelected", Tokens.EVENT));
		start.getInput().add(new Place("opponentSelected", Tokens.EVENT));
		Place moveInPlace = new Place("moveIn", Tokens.EVENT);
		start.getOutput().add(moveInPlace);

		Place movePlace = new Place("move", Tokens.MOVE);

		TaskNet moveTask = new TaskNet("move", moveInPlace, movePlace, "current");

		Transition move = new Transition(new TrueGuard(), new MoveAction());
		move.getInput().add(movePlace);
		move.getOutput().add(new Place("moved", Tokens.EVENT));

		Transition decided = new Transition(new TrueGuard(), new DecideAction());
		decided.getInput().add(new Place("decided", Tokens.DECIDE));
		decided.getOutput().add(new Place("moved", Tokens.EVENT));

		Transition moveToNext = new Transition(new TokenColorGuard(Tokens.EVENT, EventColors.NEXT.name()),
				new NextAction());
		moveToNext.getInput().add(new Place("moved", Tokens.EVENT));
		moveToNext.getOutput().add(moveInPlace);

		Workflow workflow = new Workflow();
		workflow.addTransition(init, ownerSelect, opponentSelect, start, move, moveToNext, join);
		
		workflow.addTransition(ownerSelectTask.getTransitions());
		workflow.addTransition(joinTask.getTransitions());
		workflow.addTransition(oppSelectTask.getTransitions());
		workflow.addTransition(moveTask.getTransitions());
		workflow.addTransition(decideBoth());
		workflow.addTransition(decideCurrent());
		return workflow;
	}

	private static List<Transition> decideBoth() {

		Place waitForBothCurrentPlace = new Place("waitForBothCurrent", Tokens.EVENT);
		Place waitForBothOtherPlace = new Place("waitForBothOther", Tokens.EVENT);
		Place decideBothPlace = new Place("decideBothPlace", Tokens.EVENT);

		Transition moveToDecideBoth = new Transition(new TokenColorGuard(Tokens.EVENT, EventColors.DECIDE_BOTH.name()),
				new NoOpAction());
		moveToDecideBoth.getInput().add(new Place("moved", Tokens.EVENT));
		moveToDecideBoth.getOutput().add(waitForBothCurrentPlace);
		moveToDecideBoth.getOutput().add(waitForBothOtherPlace);
		moveToDecideBoth.getOutput().add(decideBothPlace);

		Place decideBothCurrentPlace = new Place("decideBothCurrentPlace", Tokens.DECIDE);
		Place decideBothOtherPlace = new Place("decideBothOtherPlace", Tokens.DECIDE);

		TaskNet decideBothCurrentTask = new TaskNet("bothDecideCurrent", waitForBothCurrentPlace,
				decideBothCurrentPlace, "current");
		TaskNet decideBothOtherTask = new TaskNet("bothDecideOther", waitForBothOtherPlace, decideBothOtherPlace, "other");

		Transition decideBoth = new Transition(new TrueGuard(), new DecideAction());
		decideBoth.getInput().add(decideBothCurrentPlace);
		decideBoth.getInput().add(decideBothOtherPlace);
		decideBoth.getInput().add(decideBothPlace);
		decideBoth.getOutput().add(new Place("moved", Tokens.EVENT));

		List<Transition> transitions = new ArrayList<>();
		transitions.addAll(decideBothCurrentTask.getTransitions());
		transitions.addAll(decideBothOtherTask.getTransitions());
		transitions.add(decideBoth);
		transitions.add(moveToDecideBoth);
		return transitions;
	}

	private static List<Transition> decideCurrent() {

		Transition moveToDecideCurrent = new Transition(
				new TokenColorGuard(Tokens.EVENT, EventColors.DECIDE_CURRENT.name()), new NoOpAction());
		moveToDecideCurrent.getInput().add(new Place("moved", Tokens.EVENT));
		Place waitForCurrent = new Place("waitForCurrent", Tokens.EVENT);
		moveToDecideCurrent.getOutput().add(waitForCurrent);

		Place decideCurrentPlace = new Place("decideCurrent", Tokens.DECIDE);
		TaskNet task = new TaskNet("decideCurrent", waitForCurrent, decideCurrentPlace, "current");

		Transition decideCurrent = new Transition(new TrueGuard(), new DecideAction());
		decideCurrent.getInput().add(decideCurrentPlace);
		decideCurrent.getOutput().add(new Place("moved", Tokens.EVENT));

		List<Transition> transitions = new ArrayList<>();
		transitions.add(decideCurrent);
		transitions.add(moveToDecideCurrent);
		transitions.addAll(task.getTransitions());
		return transitions;
	}

	
	
}
