package eu.k5.pet.execution;

import java.util.ArrayList;
import java.util.List;

public class Execution {

	private final String id;

	private final List<Marking> markings = new ArrayList<>();

	private final List<Marking> transit = new ArrayList<>();

	private final String businessKey;

	public Execution(String id, String businessKey) {
		this.id = id;
		this.businessKey = businessKey;
	}

	public String getBusinessKey() {
		return businessKey;
	}

	void addMarking(Marking marking) {
		markings.add(marking);
	}

	// public void addMarking(Marking marking) {
	// markings.add(marking);
	// }

	public String getId() {
		return id;
	}

	public boolean hasMarking(String placeName) {
		return getMarking(placeName) != null;
	}

	public Marking getMarking(String placeName) {
		synchronized (markings) {

			for (Marking marking : markings) {
				if (marking.getPlace().getName().equals(placeName)) {
					return marking;
				}
			}
		}
		return null;
	}

	public List<Marking> getMarkings() {
		return markings;
	}

	void moveToTransit(List<Marking> input) {
		transit.addAll(input);
		markings.removeAll(input);
	}

	public Execution copy() {
		Execution exec = new Execution(id, businessKey);
		exec.markings.addAll(markings);
		exec.transit.addAll(transit);
		return exec;
	}
}
