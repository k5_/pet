package eu.k5.pet.workflow;

import java.util.ArrayList;
import java.util.List;

public class Transition {

	private List<Place> input = new ArrayList<>();

	private List<Place> output = new ArrayList<>();

	private final Guard guard;
	
	private final Action action;

	public Transition(Guard guard, Action action) {
		this.guard = guard;
		this.action = action;
	}

	public Guard getGuard() {
		return guard;
	}
	
	public Action getAction() {
		return action;
	}

	public List<Place> getInput() {
		return input;
	}

	public List<Place> getOutput() {
		return output;
	}
}
