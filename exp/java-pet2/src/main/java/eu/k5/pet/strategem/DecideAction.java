package eu.k5.pet.strategem;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.k5.pet.execution.Marking;
import eu.k5.pet.strategem.Strategem.EventColors;
import eu.k5.pet.strategem.control.MoveResult;
import eu.k5.pet.strategem.shared.Battle;
import eu.k5.pet.strategem.shared.Board;
import eu.k5.pet.strategem.shared.Piece;
import eu.k5.pet.strategem.shared.PieceType;
import eu.k5.pet.strategem.shared.Player;
import eu.k5.pet.workflow.Action;
import eu.k5.pet.workflow.ActionContext;
import eu.k5.pet.workflow.VariableValue;

public class DecideAction implements Action {
	private ObjectMapper mapper = new ObjectMapper();

	@Override
	public void execute(ActionContext context) {
		System.out.println("decide");

		List<Marking> input = context.getInput();

		VariableValue battleVar = context.getVariable("battle");
		Battle battle;
		try {
			battle = mapper.readValue(new StringReader(battleVar.getValue()), Battle.class);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		Marking event = context.getInputPlace(Tokens.EVENT);
		switch (EventColors.valueOf(event.getValue())) {
		case DECIDE_BOTH: {
			Decide current = getCurrent(context);
			Decide other = getOther(context);

			decide(battle, current.getPieceType(), other.getPieceType(), context);
			break;
		}

		case DECIDE_CURRENT: {
			Decide current = getCurrent(context);

			decide(battle, current.getPieceType(), battle.getOther(), context);
			break;
		}
		case DECIDE_OTHER:
			Decide current = getOther(context);

			decide(battle, battle.getCurrent(), current.getPieceType(), context);
		default:
			break;
		}

		// context.setOutput(Tokens.EVENT, EventColors.NEXT.name());
	}

	private void decide(Battle battle, PieceType current, PieceType other, ActionContext context) {
		EventColors result;
		if (current.equals(other)) {

			result = EventColors.DECIDE_BOTH;
		} else {
			VariableValue boardVar = context.getVariable("board");
			try {
				Board board = mapper.readValue(new StringReader(boardVar.getValue()), Board.class);

				if (current.win(other)) {

					board.addToken(battle.getLocation(), current, getCurrentPlayer(context));
					result = EventColors.NEXT;
				} else {
					board.addToken(battle.getLocation(), other, getOtherPlayer(context));
					result = EventColors.NEXT;
				}

				String asJson = mapper.writeValueAsString(board);
				context.updateVariable(boardVar, asJson);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
		context.setOutput(Tokens.EVENT, result.name());
	}

	private Decide getCurrent(ActionContext context) {
		for (Marking marking : context.getInput()) {
			if (marking.getPlace().getType().equals(Tokens.DECIDE)) {
				Decide decide = readValue(marking, Decide.class);
				if (decide.isCurrent()) {
					return decide;
				}
			}
		}
		return null;
	}

	private <T> T readValue(Marking marking, Class<T> type) {
		try {
			return mapper.readValue(new StringReader(marking.getValue()), type);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private Decide getOther(ActionContext context) {
		for (Marking marking : context.getInput()) {
			if (marking.getPlace().getType().equals(Tokens.DECIDE)) {
				Decide decide = readValue(marking, Decide.class);
				if (!decide.isCurrent()) {
					return decide;
				}
			}
		}
		return null;
	}

	private Player getCurrentPlayer(ActionContext context) {
		VariableValue variable = context.getVariable("current");
		if (Player.OWNER.name().equals(variable.getValue())) {
			return Player.OWNER;
		} else {
			return Player.OPPONENT;
		}
	}

	private Player getOtherPlayer(ActionContext context) {
		VariableValue variable = context.getVariable("current");
		if (Player.OWNER.name().equals(variable.getValue())) {
			return Player.OPPONENT;
		} else {
			return Player.OWNER;
		}
	}

}
