package eu.k5.pet.strategem;

import eu.k5.pet.strategem.Strategem.EventColors;
import eu.k5.pet.workflow.Color;
import eu.k5.pet.workflow.TokenType;

public enum Tokens implements TokenType {
	EVENT(new EnumColor(EventColors.class, EventColors.NOP, EventColors.ERR)), //
	
	INIT(new JsonColor()), //
	JOIN(new JsonColor()),
	SELECT(new JsonColor()), //
	MOVE(null),
	DECIDE(new JsonColor());
	

	private Tokens(Color color) {

	}
	
	@Override
	public String initValue() {
		return "";
	}
}