package eu.k5.pet.workflow;

import java.time.Instant;

public interface Guard {

	boolean canPass(GuardContext context);

	default Instant retest(GuardContext context) {
		return null;
	}

}
