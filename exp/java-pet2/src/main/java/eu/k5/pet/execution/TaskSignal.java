package eu.k5.pet.execution;

public class TaskSignal {

	private final String key;
	private final String signalPlace;
	private final String owner;
	private final Object value;

	public TaskSignal(String key, String signalPlace, String owner, Object value) {
		this.key = key;
		this.signalPlace = signalPlace;
		this.owner = owner;
		this.value = value;
	}

	public String getSignalPlace() {
		return signalPlace;
	}

	public String getKey() {
		return key;
	}

	public String getOwner() {
		return owner;
	}

	public Object getValue() {
		return value;
	}
}
