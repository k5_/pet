package eu.k5.exp.mongo;

import java.net.UnknownHostException;
import java.util.Date;

import org.bson.Document;

import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

public class MongoMain {
	public static void main(String[] args) throws UnknownHostException {
		//Mongo mongo = new Mongo("localhost", 27017);

		// Since 2.10.0, uses MongoClient
		MongoClient mongo = new MongoClient("localhost", 27017);
		
		
		/**** Get database ****/
		// if database doesn't exists, MongoDB will create it for you
		MongoDatabase db = mongo.getDatabase("testdb");

		/**** Get collection / table from 'testdb' ****/
		// if collection doesn't exists, MongoDB will create it for you
		MongoCollection<Document> table = db.getCollection("user");
		
		
		Document document = new Document();
		document.put("name", "mkyong");
		document.put("age", 30);
		document.put("createdDate", new Date());
		table.insertOne(document);
		
		
		
		Document searchQuery = new Document();
		searchQuery.put("name", "mkyong");

		
		
		FindIterable<Document> cursor = table.find(searchQuery);
		
		MongoCursor<Document> it = cursor.iterator();
		while (it.hasNext()) {
			System.out.println(it.next());
		}
		
	}
}
