package eu.k5.strategem.cell.shared;

import org.jboss.errai.common.client.api.annotations.Portable;

import eu.k5.cell.annotation.Task;
import eu.k5.strategem.shared.Player;

@Portable
public class OwnerAspect extends StrategemAspect {

	@Task(definition = "OwnerSelectFlag")
	private SelectFlag selectFlag;

	@Override
	public SelectFlag getSelectFlag() {
		return selectFlag;
	}

	public void setSelectFlag(SelectFlag selectFlag) {
		this.selectFlag = selectFlag;
	}

	@Override
	public Player getPlayer() {
		return Player.OWNER;
	}

}
