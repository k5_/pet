package eu.k5.strategem.shared;

import org.jboss.errai.common.client.api.annotations.MapsTo;
import org.jboss.errai.common.client.api.annotations.Portable;

@Portable
public class GameReference {

	private final String game;
	private final String user;
	private final boolean owner;

	public GameReference(@MapsTo("game") String game, @MapsTo("user") String user, @MapsTo("owner") boolean owner) {
		this.game = game;
		this.user = user;
		this.owner = owner;
	}

	public static GameReference forOwner(String gameKey, String user) {
		return new GameReference(gameKey, user, true);
	}

	public static GameReference forOpponent(String gameKey, String user) {
		return new GameReference(gameKey, user, false);
	}

	public String getGame() {
		return game;
	}

	public String getUser() {
		return user;
	}

	public boolean isOwner() {
		return owner;
	}

	@Override
	public String toString() {
		return "GameReference [game=" + game + ", user=" + user + ", isOwner=" + owner + "]";
	}

}
