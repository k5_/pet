package eu.k5.strategem.cell.shared;

import org.jboss.errai.common.client.api.annotations.Portable;

import eu.k5.cell.annotation.CellReference;
import eu.k5.cell.shared.CellTaskReference;

@Portable
public class WinTask {
	@CellReference
	private CellTaskReference taskReference;

	public CellTaskReference getTaskReference() {
		return taskReference;
	}

	public void setTaskReference(CellTaskReference taskReference) {
		this.taskReference = taskReference;
	}

}
