package eu.k5.strategem.shared;

import java.util.List;
import java.util.Map;

import org.jboss.errai.bus.server.annotations.Remote;

import eu.k5.strategem.cell.shared.Decide;
import eu.k5.strategem.cell.shared.MoveTask;
import eu.k5.strategem.cell.shared.SelectFlag;
import eu.k5.strategem.cell.shared.StrategemAspect;

@Remote
public interface StrategemProxy {

	GameReference start(String gameName, Layout layout, boolean isPublic);

	GameReference join(String game);

	StrategemAspect getView(GameReference reference);

	void selectFlag(GameReference game, SelectFlag selectFlag);

	void move(GameReference game, MoveTask move);

	void decide(Decide decide);

	StrategemPublic getPublicData(String game);

	List<StrategemPublic> getPublicGames();

}
