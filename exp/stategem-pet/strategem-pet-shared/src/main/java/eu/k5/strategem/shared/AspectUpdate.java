package eu.k5.strategem.shared;

import org.jboss.errai.common.client.api.annotations.MapsTo;
import org.jboss.errai.common.client.api.annotations.Portable;
import org.jboss.errai.enterprise.client.cdi.api.Conversational;

import eu.k5.strategem.cell.shared.StrategemAspect;

@Conversational
@Portable
public class AspectUpdate {

	private final StrategemAspect aspect;

	public AspectUpdate(@MapsTo("aspect") StrategemAspect aspect) {
		this.aspect = aspect;
	}

	public StrategemAspect getAspect() {
		return aspect;
	}
}
