package eu.k5.strategem.cell.shared;

import org.jboss.errai.common.client.api.annotations.Portable;

import eu.k5.cell.annotation.Out;
import eu.k5.strategem.shared.TokenType;

@Portable
public class DecideOther extends Decide {
	@Out
	private TokenType decideOther;

	@Override
	public TokenType getDecision() {
		return decideOther;
	}

	@Override
	public void setDecision(TokenType decision) {
		decideOther = decision;
	}

}
