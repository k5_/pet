package eu.k5.strategem.shared;

import java.io.Serializable;

import org.jboss.errai.common.client.api.annotations.MapsTo;
import org.jboss.errai.common.client.api.annotations.Portable;

@Portable
public class Location implements Serializable {
	private static final long serialVersionUID = 1L;
	private final int row;
	private final int column;

	public Location(@MapsTo("row") int row, @MapsTo("column") int column) {
		this.row = row;
		this.column = column;
	}

	public int getRow() {
		return row;
	}

	public int getColumn() {
		return column;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + column;
		result = prime * result + row;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Location other = (Location) obj;
		if (column != other.column) {
			return false;
		}
		if (row != other.row) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Location [row=" + row + ", column=" + column + "]";
	}

	public Location move(Direction movement) {
		switch (movement) {
		case DOWN:
			return new Location(getRow() + 1, getColumn());
		case UP:
			return new Location(getRow() - 1, getColumn());
		case LEFT:
			return new Location(getRow(), getColumn() - 1);
		case RIGHT:
			return new Location(getRow(), getColumn() + 1);
		default:
			return new Location(getRow(), getColumn());
		}
	}

	public boolean equals(int row, int column) {
		return this.row == row && this.column == column;
	}

}
