package eu.k5.strategem.cell.shared;

import org.jboss.errai.common.client.api.annotations.Portable;

import eu.k5.cell.annotation.CellReference;
import eu.k5.cell.annotation.In;
import eu.k5.cell.annotation.Out;
import eu.k5.cell.shared.CellTaskReference;

@Portable
public class JoinGameTask {

	@CellReference
	private CellTaskReference taskReference;
	@In
	private String game;
	@Out
	private String opponent;
	@Out
	private String other;
	@Out
	private String opponentName;

	public String getGame() {
		return game;
	}

	public void setGame(String game) {
		this.game = game;
	}

	public String getOpponent() {
		return opponent;
	}

	public void setOpponent(String opponent) {
		this.opponent = opponent;
		other = opponent;
	}

	public CellTaskReference getTaskReference() {
		return taskReference;
	}

	public void setTaskReference(CellTaskReference taskReference) {
		this.taskReference = taskReference;
	}

	public void setOpponentName(String name) {
		opponentName = name;

	}

	public String getOpponentName() {
		return opponentName;
	}

}
