package eu.k5.strategem.shared;

import org.jboss.errai.common.client.api.annotations.MapsTo;
import org.jboss.errai.common.client.api.annotations.Portable;

@Portable
public class RequestAspect {

	private final GameReference game;

	public RequestAspect(@MapsTo("game") GameReference game) {
		this.game = game;
	}

	public GameReference getGame() {
		return game;
	}

}
