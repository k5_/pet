package eu.k5.strategem.shared;

import org.jboss.errai.common.client.api.annotations.Portable;

@Portable
public enum TokenType {
	UNSPECIFIED {
		@Override
		public boolean win(TokenType type) {
			return false;
		}
	},
	ROCK {
		@Override
		public boolean win(TokenType type) {
			return type.equals(SCISSORS) || type.equals(LIZARD);
		}
	},
	PAPER {
		@Override
		public boolean win(TokenType type) {
			return type.equals(ROCK) || type.equals(SPOCK);
		}
	},
	SCISSORS {
		@Override
		public boolean win(TokenType type) {
			return type.equals(PAPER) || type.equals(LIZARD);
		}
	},
	LIZARD {
		@Override
		public boolean win(TokenType type) {
			return type.equals(SPOCK) || type.equals(PAPER);
		}
	},
	SPOCK {
		@Override
		public boolean win(TokenType type) {
			return type.equals(ROCK) || type.equals(SCISSORS);
		}
	},
	NONE {
		@Override
		public boolean win(TokenType type) {
			return false;
		}
	},
	FLAG {
		@Override
		public boolean win(TokenType type) {
			return false;
		}

	},
	CAPTURED_FLAG {
		@Override
		public boolean win(TokenType type) {
			return false;
		}

	},
	BATTLE {
		@Override
		public boolean win(TokenType type) {
			return false;
		}
	};

	public abstract boolean win(TokenType type);

}
