package eu.k5.strategem.cell.shared;

import eu.k5.cell.annotation.CellReference;
import eu.k5.cell.annotation.In;
import eu.k5.cell.shared.CellTaskReference;
import eu.k5.strategem.shared.Location;
import eu.k5.strategem.shared.TokenType;

public abstract class Decide {
	@CellReference
	private CellTaskReference taskReference;
	@In
	private String game;
	@In
	private Location battle;

	public Location getBattle() {
		return battle;
	}

	public void setBattle(Location battle) {
		this.battle = battle;
	}

	public abstract TokenType getDecision();

	public abstract void setDecision(TokenType decision);

	public String getGame() {
		return game;
	}

	public void setGame(String game) {
		this.game = game;
	}

}
