package eu.k5.strategem.cell.shared;

import eu.k5.cell.annotation.In;
import eu.k5.cell.annotation.Task;
import eu.k5.strategem.shared.Board;
import eu.k5.strategem.shared.Player;

public abstract class StrategemAspect {

	@In
	private Board board;

	@In
	private String gameName;
	@In
	private String ownerName;
	@In
	private String opponentName;

	@Task
	private MoveTask move;

	@Task
	private DecideCurrent decideCurrent;

	@Task
	private DecideOther decideOther;

	@Task
	private WinTask win;

	@Task
	private LostTask lost;

	public abstract Player getPlayer();

	public abstract SelectFlag getSelectFlag();

	public MoveTask getMove() {
		return move;
	}

	public void setMove(MoveTask move) {
		this.move = move;
	}

	public Decide getDecide() {
		if (decideCurrent != null) {
			return decideCurrent;
		}
		return decideOther;
	}

	public WinTask getWin() {
		return win;
	}

	public void setWin(WinTask win) {
		this.win = win;
	}

	public LostTask getLost() {
		return lost;
	}

	public void setLost(LostTask lost) {
		this.lost = lost;
	}

	public boolean isWaiting() {
		return move == null && decideCurrent == null && decideOther == null && win == null && lost == null;
	}

	public Board getBoard() {
		return board;
	}

	public void setBoard(Board board) {
		this.board = board;
	}

	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getOpponentName() {
		return opponentName;
	}

	public void setOpponentName(String opponentName) {
		this.opponentName = opponentName;
	}

}
