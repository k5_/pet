package eu.k5.strategem.shared.composite;

import java.util.List;

import org.jboss.errai.common.client.api.annotations.MapsTo;
import org.jboss.errai.common.client.api.annotations.Portable;

@Portable
public class Group implements Item {
	private final List<Item> subItems;

	public Group(@MapsTo("subItems") List<Item> subItems) {
		this.subItems = subItems;
	}

	public List<Item> getSubItems() {

		return subItems;
	}
}
