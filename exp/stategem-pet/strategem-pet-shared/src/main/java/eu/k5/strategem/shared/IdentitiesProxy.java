package eu.k5.strategem.shared;

import org.jboss.errai.bus.server.annotations.Remote;

@Remote
public interface IdentitiesProxy {

	IdentityState getOrCreateIdentity();

	IdentityState changeName(String name);
}
