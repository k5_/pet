package eu.k5.cell.annotation;

public @interface Task {
	String definition() default "";
}
