package eu.k5.strategem.cell.shared;

import org.jboss.errai.common.client.api.annotations.Portable;

import eu.k5.cell.annotation.Out;
import eu.k5.strategem.shared.TokenType;

@Portable
public class DecideCurrent extends Decide {
	@Out
	private TokenType decideCurrent;

	@Override
	public TokenType getDecision() {
		return decideCurrent;
	}

	@Override
	public void setDecision(TokenType decision) {
		decideCurrent = decision;
	}

}
