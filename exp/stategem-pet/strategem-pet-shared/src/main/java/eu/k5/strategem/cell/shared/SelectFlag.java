package eu.k5.strategem.cell.shared;

import org.jboss.errai.common.client.api.annotations.Portable;

import eu.k5.cell.annotation.CellReference;
import eu.k5.cell.annotation.Out;
import eu.k5.cell.shared.CellTaskReference;
import eu.k5.strategem.shared.Location;

@Portable
public class SelectFlag {
	@CellReference
	private CellTaskReference taskReference;

	@Out
	private Location tempLocation;

	public void setTaskReference(CellTaskReference taskReference) {
		this.taskReference = taskReference;
	}

	public Location getLocation() {
		return tempLocation;
	}

	public void setLocation(Location location) {
		tempLocation = location;
	}

}