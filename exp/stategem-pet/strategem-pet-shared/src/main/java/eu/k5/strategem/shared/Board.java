package eu.k5.strategem.shared;

import java.io.Serializable;

import org.jboss.errai.common.client.api.annotations.MapsTo;
import org.jboss.errai.common.client.api.annotations.Portable;

@Portable
public class Board implements Serializable {
	private static final long serialVersionUID = 1L;

	private final int rows;
	private final int columns;
	private final Token[][] tokens;

	private Location lastBattle;

	public Board(@MapsTo("rows") int rows, @MapsTo("columns") int columns, @MapsTo("tokens") Token[][] tokens) {
		this.rows = rows;
		this.columns = columns;
		this.tokens = tokens;
		lastBattle = lastBattle;
	}

	public Token removeToken(Location location) {
		if (!isValid(location)) {
			throw new IllegalArgumentException();
		}

		Token token = tokens[location.getRow()][location.getColumn()];
		tokens[location.getRow()][location.getColumn()] = null;
		return token;
	}

	public int getRows() {
		return rows;
	}

	public int getColumns() {
		return columns;
	}

	public boolean isValid(int row, int column) {
		return row >= 0 && row < rows && column >= 0 && column < columns;
	}

	public boolean isValid(Location location) {
		return isValid(location.getRow(), location.getColumn());
	}

	public Player getOwner(int row, int column) {
		Token token = getTokenAt(row, column);
		if (token == null) {
			return null;
		} else {
			return token.getOwner();
		}
	}

	public Token getTokenAt(Location location) {
		return getTokenAt(location.getRow(), location.getColumn());
	}

	public Token getTokenAt(int row, int column) {
		return tokens[row][column];
	}

	public void addToken(Location location, TokenType type, Player player) {
		Token token = new Token(location, type, player);
		tokens[location.getRow()][location.getColumn()] = token;
	}

	public void setToken(Token token) {
		tokens[token.getLocation().getRow()][token.getLocation().getColumn()] = token;
	}

	public Location getLastBattle() {
		return lastBattle;
	}

	public boolean isLastBattle(int row, int column) {
		return new Location(row, column).equals(lastBattle);
	}

	public void setLastBattle(Location battle) {
		lastBattle = battle;
	}

}
