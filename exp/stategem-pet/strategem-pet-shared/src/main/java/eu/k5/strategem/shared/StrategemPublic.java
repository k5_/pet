package eu.k5.strategem.shared;

import java.util.Date;

import org.jboss.errai.common.client.api.annotations.Portable;

import eu.k5.cell.annotation.In;

@Portable
public class StrategemPublic {

	@In
	private String game;
	@In
	private String gameName;

	@In
	private Date started;
	@In
	private String ownerName;

	@In
	private String opponentName;
	@In
	private int rows;
	@In
	private int columns;

	public String getGame() {
		return game;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public String getOpponentName() {
		return opponentName;
	}

	public int getRows() {
		return rows;
	}

	public int getColumns() {
		return columns;
	}

	public void setGame(String game) {
		this.game = game;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public void setOpponentName(String opponentName) {
		this.opponentName = opponentName;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public void setColumns(int columns) {
		this.columns = columns;
	}

	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	public Date getStarted() {
		return started;
	}

	public void setStarted(Date started) {
		this.started = started;
	}

}
