package eu.k5.strategem.shared;

import java.io.Serializable;

import org.jboss.errai.common.client.api.annotations.MapsTo;
import org.jboss.errai.common.client.api.annotations.Portable;

@Portable
public class Token implements Serializable {
	private static final long serialVersionUID = 1L;

	private final Location location;
	private final TokenType type;
	private final Player owner;

	public Token(@MapsTo("location") Location location, @MapsTo("type") TokenType type, @MapsTo("owner") Player owner) {
		this.location = location;
		this.type = type;
		this.owner = owner;
	}

	public Location getLocation() {
		return location;
	}

	public TokenType getType() {
		return type;
	}

	public Player getOwner() {
		return owner;
	}

	@Override
	public String toString() {
		return "Token [location=" + location + ", type=" + type + ", owner=" + owner + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (location == null ? 0 : location.hashCode());
		result = prime * result + (owner == null ? 0 : owner.hashCode());
		result = prime * result + (type == null ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Token other = (Token) obj;
		if (location == null) {
			if (other.location != null) {
				return false;
			}
		} else if (!location.equals(other.location)) {
			return false;
		}
		if (owner != other.owner) {
			return false;
		}
		if (type != other.type) {
			return false;
		}
		return true;
	}

}
