package eu.k5.strategem.shared;

import org.jboss.errai.common.client.api.annotations.Portable;

@Portable
public enum Player {
	OWNER, OPPONENT, BATTLE;
}
