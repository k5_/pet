package eu.k5.strategem.cell.shared;

import org.jboss.errai.common.client.api.annotations.Portable;

import eu.k5.cell.annotation.CellReference;
import eu.k5.cell.annotation.In;
import eu.k5.cell.annotation.Out;
import eu.k5.cell.shared.CellTaskReference;
import eu.k5.strategem.shared.Direction;
import eu.k5.strategem.shared.Location;

@Portable
public class MoveTask {

	@In
	private String game;

	@Out
	private Location tempLocation;

	@Out
	private Direction tempDirection;

	@CellReference
	private CellTaskReference taskReference;

	public String getGame() {
		return game;
	}

	public void setGame(String game) {
		this.game = game;
	}

	public Location getSource() {
		return tempLocation;
	}

	public void setSource(Location source) {
		this.tempLocation = source;
	}

	public Direction getMovement() {
		return tempDirection;
	}

	public void setMovement(Direction movement) {
		this.tempDirection = movement;
	}

	public CellTaskReference getTaskReference() {
		return taskReference;
	}

	public void setTaskReference(CellTaskReference taskReference) {
		this.taskReference = taskReference;
	}

}
