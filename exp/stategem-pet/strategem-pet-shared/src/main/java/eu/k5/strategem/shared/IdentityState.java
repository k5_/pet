package eu.k5.strategem.shared;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;

import org.jboss.errai.common.client.api.annotations.MapsTo;
import org.jboss.errai.common.client.api.annotations.Portable;

@Portable
@SessionScoped
public class IdentityState implements Serializable {
	private static final long serialVersionUID = 1L;

	private final String uuid;
	private final String name;

	public IdentityState(@MapsTo("uuid") String uuid, @MapsTo("name") String name) {
		this.uuid = uuid;
		this.name = name;
	}

	public String getUuid() {
		return uuid;
	}

	public String getName() {
		return name;
	}

}
