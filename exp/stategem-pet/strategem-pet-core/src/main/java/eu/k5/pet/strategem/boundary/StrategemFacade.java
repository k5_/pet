package eu.k5.pet.strategem.boundary;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.k5.pet.execution.Job;
import eu.k5.pet.strategem.StrategemStart;
import eu.k5.pet.strategem.control.StratEngine;
import eu.k5.strategem.cell.shared.Decide;
import eu.k5.strategem.cell.shared.MoveTask;
import eu.k5.strategem.cell.shared.SelectFlag;
import eu.k5.strategem.cell.shared.StrategemAspect;
import eu.k5.strategem.shared.GameReference;
import eu.k5.strategem.shared.IdentityState;
import eu.k5.strategem.shared.Layout;
import eu.k5.strategem.shared.StrategemProxy;
import eu.k5.strategem.shared.StrategemPublic;

@Singleton
public class StrategemFacade implements StrategemProxy {
	private static final Logger LOGGER = LoggerFactory.getLogger(StrategemFacade.class);
	private static final ObjectMapper MAPPER = new ObjectMapper();
	@Inject
	Identity identity;

	@Inject
	private StratEngine engine;

	@Override
	public GameReference start(String gameName, Layout layout, boolean isPublic) {
		LOGGER.info("Starting process {}", gameName);
		String gameKey = UUID.randomUUID().toString();
		IdentityState state = identity.getState();

		StrategemStart start = new StrategemStart();
		start.setGame(gameKey);
		start.setOwner("w_" + state.getUuid());
		start.setOwnerName(state.getName());
		start.setGameName(gameName);
		start.setStarted(new Date());
		start.setOpen(isPublic);
		start.setRows(layout.getRows());
		start.setColumns(layout.getColumns());

		Job job;
		try {
			job = new Job(gameKey, MAPPER.writeValueAsString(start));
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}

		engine.start(job);

		return GameReference.forOwner(gameKey, "w_" + state.getUuid());
	}

	@Override
	public GameReference join(String game) {

		engine.getTask(game, )
		return null;
	}

	@Override
	public StrategemAspect getView(GameReference reference) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void selectFlag(GameReference game, SelectFlag selectFlag) {
		// TODO Auto-generated method stub

	}

	@Override
	public void move(GameReference game, MoveTask move) {
		// TODO Auto-generated method stub

	}

	@Override
	public void decide(Decide decide) {
		// TODO Auto-generated method stub

	}

	@Override
	public StrategemPublic getPublicData(String game) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<StrategemPublic> getPublicGames() {

		return new ArrayList<>();
	}

}
