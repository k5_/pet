package eu.k5.pet.strategem.control;

import java.time.Clock;
import java.util.concurrent.Executors;

import javax.annotation.PostConstruct;
import javax.inject.Singleton;

import eu.k5.pet.execution.Engine;
import eu.k5.pet.execution.Job;
import eu.k5.pet.execution.Repository;
import eu.k5.pet.execution.TaskScheduler;
import eu.k5.pet.strategem.Strategem;
import eu.k5.pet.workflow.Workflow;

@Singleton
public class StratEngine {

	private Engine engine;

	@PostConstruct
	public void init() {
		Workflow workflow = Strategem.createWorkflow();
		Repository repository = new Repository();
		TaskScheduler scheduler = new TaskScheduler(Clock.systemUTC(), Executors.newScheduledThreadPool(1));
		engine = Engine.builder().withRepository(repository).withWorkflow(workflow).withScheduler(scheduler).build();

	}
	
	public void start(Job job){
		engine.create(job);
	}
}
