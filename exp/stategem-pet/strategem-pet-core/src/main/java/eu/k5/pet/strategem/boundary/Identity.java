package eu.k5.pet.strategem.boundary;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;

import eu.k5.strategem.shared.IdentityState;

@SessionScoped
public class Identity implements Serializable {
	private static final long serialVersionUID = 1L;

	private IdentityState state;

	public Identity() {

	}

	public Identity(IdentityState state) {
		this.state = state;
	}

	public IdentityState getState() {
		return state;
	}

	public void setState(IdentityState state) {
		this.state = state;
	}

}
