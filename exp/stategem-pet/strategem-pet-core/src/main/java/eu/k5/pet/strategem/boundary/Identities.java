package eu.k5.pet.strategem.boundary;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.jboss.errai.bus.server.annotations.Service;

import eu.k5.strategem.shared.IdentitiesProxy;
import eu.k5.strategem.shared.IdentityState;

@Service
@ApplicationScoped
public class Identities implements IdentitiesProxy {

	private AtomicInteger nameGen;

	@Inject
	Identity identity;

	@PostConstruct
	public void init() {
		nameGen = new AtomicInteger();
	}

	@Override
	public IdentityState getOrCreateIdentity() {
		IdentityState state = identity.getState();
		if (state == null) {
			state = new IdentityState(UUID.randomUUID().toString(), "guest_" + nameGen.incrementAndGet());
			identity.setState(state);
		}
		return state;
	}

	@Override
	public IdentityState changeName(String name) {
		IdentityState state = identity.getState();
		IdentityState newState = new IdentityState(state.getUuid(), name);

		identity.setState(state);
		return newState;

	}

}
