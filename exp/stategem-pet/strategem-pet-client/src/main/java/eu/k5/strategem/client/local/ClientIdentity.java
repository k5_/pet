package eu.k5.strategem.client.local;

import javax.inject.Singleton;

import eu.k5.strategem.shared.IdentityState;

@Singleton
public class ClientIdentity {
	private IdentityState state;

	public IdentityState getState() {
		return state;
	}

	public void setState(IdentityState state) {
		this.state = state;
	}

}
