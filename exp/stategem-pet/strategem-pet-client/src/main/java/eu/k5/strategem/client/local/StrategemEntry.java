package eu.k5.strategem.client.local;

import javax.annotation.PostConstruct;

import org.jboss.errai.common.client.api.Caller;
import org.jboss.errai.common.client.api.RemoteCallback;
import org.jboss.errai.ioc.client.api.AfterInitialization;
import org.jboss.errai.ioc.client.api.EntryPoint;

import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.inject.Inject;

import eu.k5.strategem.shared.IdentitiesProxy;
import eu.k5.strategem.shared.IdentityState;

@EntryPoint
public class StrategemEntry {

	@Inject
	private AppController appController;

	@PostConstruct
	public void init() {
		History.addValueChangeHandler(appController);
	}

	@Inject
	ClientIdentity identity;

	@Inject
	private Caller<IdentitiesProxy> identities;

	@AfterInitialization
	public void start() {

		identities.call(new RemoteCallback<IdentityState>() {

			@Override
			public void callback(IdentityState response) {
				identity.setState(response);
				appController.go(RootLayoutPanel.get());

			}
		}).getOrCreateIdentity();

	}
}
