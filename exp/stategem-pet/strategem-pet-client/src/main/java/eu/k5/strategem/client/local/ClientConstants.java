package eu.k5.strategem.client.local;

public abstract class ClientConstants {

	public static final String PARAM_GAME = "game";
	public static final String PARAM_USER = "user";
	public static final String PARAM_OWNER = "owner";

	public static final int ROWS = 7;
	public static final int COLUMNS = 7;

	private ClientConstants() {

	}
}
