package eu.k5.strategem.client.local.Game.board;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Image;

import eu.k5.strategem.client.local.Action;
import eu.k5.strategem.shared.Direction;
import eu.k5.strategem.shared.TokenType;

public class TokenWidget extends Grid {

	private final List<MoveWidget> movements = new ArrayList<MoveWidget>();

	private final Image icon;

	private Action<Direction> moveAction;
	private Action<Void> selectAction;

	private final TokenResource own;
	private final TokenResource other;
	private final Movements resources;

	private Set<Direction> allowed;

	private boolean movementActive;

	public TokenWidget(TokenResource own, TokenResource other, Movements resources) {
		super(3, 3);
		this.own = own;
		this.other = other;

		this.resources = resources;

		{
			MoveWidget widget = new MoveWidget(resources.moveUp(), resources.noneHorizontal(), Direction.UP);
			setWidget(0, 1, widget);
			movements.add(widget);
		}
		{
			MoveWidget widget = new MoveWidget(resources.moveDown(), resources.noneHorizontal(), Direction.DOWN);
			setWidget(2, 1, widget);
			movements.add(widget);
		}
		{
			MoveWidget widget = new MoveWidget(resources.moveLeft(), resources.noneVertical(), Direction.LEFT);
			setWidget(1, 0, widget);
			movements.add(widget);
		}
		{
			MoveWidget widget = new MoveWidget(resources.moveRight(), resources.noneVertical(), Direction.RIGHT);
			setWidget(1, 2, widget);
			movements.add(widget);
		}

		icon = new Image();
		icon.setHeight("70px");
		icon.setWidth("70px");
		icon.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				for (MoveWidget widget : movements) {
					widget.enable(false);
				}
				selectAction.act(null);
			}
		});
		setWidget(1, 1, icon);
		addMouseOverHandler(new MouseOverHandler() {

			@Override
			public void onMouseOver(MouseOverEvent event) {
				if (movementActive) {
					for (MoveWidget widget : movements) {
						if (allowed.contains(widget.getType())) {
							widget.enable(true);
						} else {
							widget.enable(false);
						}
					}
				}
			}
		});

		addMouseOutHandler(new MouseOutHandler() {

			@Override
			public void onMouseOut(MouseOutEvent event) {

				for (MoveWidget widget : movements) {

					widget.enable(false);
				}
			}
		});
	}

	public HandlerRegistration addMouseOverHandler(MouseOverHandler handler) {
		return addDomHandler(handler, MouseOverEvent.getType());
	}

	public HandlerRegistration addMouseOutHandler(MouseOutHandler handler) {
		return addDomHandler(handler, MouseOutEvent.getType());
	}

	class MoveWidget extends Image {

		private final Direction type;
		private final ImageResource enabled;
		private final ImageResource disabled;
		private boolean isEnabled;

		public MoveWidget(ImageResource enabled, ImageResource disabled, final Direction type) {
			super(disabled);
			this.enabled = enabled;
			this.disabled = disabled;
			this.type = type;

			addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					if (isEnabled && movementActive) {
						moveAction.act(type);
					}
				}
			});
		}

		public void enable(boolean state) {
			isEnabled = state;
			if (state) {
				this.setResource(enabled);
			} else {
				this.setResource(disabled);
			}
		}

		public Direction getType() {
			return type;
		}
	}

	void setAllowedMovements(Set<Direction> allowed) {
		this.allowed = allowed;

	}

	void setMoveAction(Action<Direction> action) {
		moveAction = action;
	}

	void setToken(TokenType type, boolean isOwn, Set<Direction> allowed) {
		TokenResource resource = isOwn ? own : other;
		setToken(type, resource);
		setAllowedMovements(allowed);
	}

	public void setToken(TokenType type, TokenResource resource) {
		switch (type) {
		case LIZARD:
			setIcon(resource.lizard());
			return;
		case PAPER:
			setIcon(resource.paper());
			return;
		case ROCK:
			setIcon(resource.rock());
			return;
		case SPOCK:
			setIcon(resource.spock());
			return;
		case SCISSORS:
			setIcon(resource.scissors());
			return;
		case FLAG:
			setIcon(resource.flag());
			return;
		case CAPTURED_FLAG:
			setIcon(resource.capturedFlag());
			return;
		case UNSPECIFIED:
			setIcon(resource.unspecified());
			return;
		case NONE:
			setIcon(resource.none());
			return;

		case BATTLE:
			setIcon(resource.battle());
			return;
		default:
			break;
		}
	}

	private void setIcon(ImageResource resource) {
		icon.setHeight("70px");
		icon.setWidth("70px");
		icon.setResource(resource);
	}

	public void setSelectAction(Action<Void> action) {
		selectAction = action;
	}

	public void setMovementActive(boolean state) {
		movementActive = state;
	}

}
