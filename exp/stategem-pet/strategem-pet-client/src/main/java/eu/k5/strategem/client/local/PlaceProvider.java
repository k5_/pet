package eu.k5.strategem.client.local;

import javax.enterprise.inject.Produces;
import javax.inject.Singleton;

import org.jboss.errai.ui.nav.client.local.HistoryToken;

import com.google.gwt.user.client.History;

@Singleton
public class PlaceProvider {

	@Produces
	public HistoryToken token() {
		return HistoryToken.parse(History.getToken());
	}
}
