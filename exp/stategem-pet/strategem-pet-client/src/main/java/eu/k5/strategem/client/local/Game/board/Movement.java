package eu.k5.strategem.client.local.Game.board;

import eu.k5.strategem.shared.Direction;
import eu.k5.strategem.shared.Location;

public class Movement {
	private final Location source;
	private final Direction direction;

	public Movement(Location source, Direction direction) {
		this.source = source;
		this.direction = direction;
	}

	public Location getSource() {
		return source;
	}

	public Direction getDirection() {
		return direction;
	}

	@Override
	public String toString() {
		return "Movement [source=" + source + ", direction=" + direction + "]";
	}

}
