package eu.k5.strategem.client.local.Lobby;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import org.jboss.errai.common.client.api.Caller;
import org.jboss.errai.common.client.api.RemoteCallback;

import com.google.gwt.user.client.ui.HasWidgets;

import eu.k5.strategem.client.local.ClientConstants;
import eu.k5.strategem.client.local.ClientIdentity;
import eu.k5.strategem.client.local.Navigator;
import eu.k5.strategem.client.local.Presenter;
import eu.k5.strategem.client.local.View;
import eu.k5.strategem.shared.GameReference;
import eu.k5.strategem.shared.Layout;
import eu.k5.strategem.shared.StrategemProxy;
import eu.k5.strategem.shared.StrategemPublic;

@Dependent
public class Lobby implements Presenter {

	public interface LobbyView extends View<Lobby> {

		JoinView getJoin();

		StartView getStart();

		void setGames(List<StrategemPublic> games);
	}

	public interface JoinView extends View<Lobby> {

		void setOwnerName(String name);

		void setJoinable(boolean state);

		void setObservable(boolean state);

		void setRows(int rows);

		void setColumns(int columns);

		String getGameKey();

		void setGameKey(String game);

		void setInformationVisible(boolean state);
	}

	public interface StartView extends View<Lobby> {

		String getName();

		void setOwnerName(String name);

		Layout getLayout();

		boolean isPublic();

	}

	@Inject
	private LobbyView view;

	@Inject
	private Navigator navigator;

	@Inject
	Caller<StrategemProxy> strategems;

	@Inject
	ClientIdentity identity;

	@PostConstruct
	public void init() {
		view.setPresenter(this);
	}

	@Override
	public void display(HasWidgets container) {
		String game = navigator.getParameter("game");

		if (game != null) {
			gameSelected(game);
		}

		view.getJoin().setJoinable(false);
		view.getJoin().setObservable(false);

		view.getStart().setOwnerName(identity.getState().getName());

		strategems.call(new RemoteCallback<List<StrategemPublic>>() {

			@Override
			public void callback(List<StrategemPublic> games) {
				view.setGames(games);
			}
		}).getPublicGames();

		container.clear();
		container.add(view.asWidget());
	}

	void gameSelected(String game) {
		strategems.call(new RemoteCallback<StrategemPublic>() {
			@Override
			public void callback(StrategemPublic response) {
				setJoinData(response);
			}
		}).getPublicData(game);
		view.getJoin().setInformationVisible(false);
	}

	void addActiveGames(List<StrategemPublic> games) {
		view.setGames(games);
	}

	void start() {
		StartView start = view.getStart();
		Layout layout = start.getLayout();
		if (layout == null) {
			layout = Layout._4x3;
		}

		strategems.call(new RemoteCallback<GameReference>() {
			@Override
			public void callback(GameReference response) {
				navigator.to("game", ClientConstants.PARAM_GAME, response.getGame(), ClientConstants.PARAM_USER,
						response.getUser(), "owner", "true");
			}
		}).start(start.getName(), layout, start.isPublic());
	}

	public void setJoinData(StrategemPublic data) {
		JoinView join = view.getJoin();
		if (data == null) {
			join.setInformationVisible(false);
			return;
		}
		join.setGameKey(data.getGame());
		join.setOwnerName(data.getOwnerName());
		join.setRows(data.getRows());
		join.setColumns(data.getColumns());
		join.setJoinable(data.getOpponentName() == null);
		join.setObservable(false);
		join.setInformationVisible(true);
	}

	public void join(String game) {
		strategems.call(new RemoteCallback<GameReference>() {

			@Override
			public void callback(GameReference response) {
				navigator.to("game", ClientConstants.PARAM_GAME, response.getGame(), ClientConstants.PARAM_USER,
						response.getUser(), "owner", "false");
			}
		}).join(game);
	}

}
