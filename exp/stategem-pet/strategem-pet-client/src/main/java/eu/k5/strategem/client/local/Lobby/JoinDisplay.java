package eu.k5.strategem.client.local.Lobby;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import eu.k5.strategem.client.local.Lobby.Lobby.JoinView;

@Dependent
public class JoinDisplay extends Composite implements JoinView {

	@Inject
	UiBinder<VerticalPanel, JoinDisplay> binder;

	@UiField
	public Button join;

	@UiField
	public TextBox gameKey;

	@UiField
	public Label ownerName;

	@UiField
	public Label columns;

	@UiField
	public Label rows;

	@UiField
	public Grid gameInformation;

	private Lobby presenter;

	@PostConstruct
	public void init() {
		initWidget(binder.createAndBindUi(this));
	}

	@Override
	public void setPresenter(Lobby presenter) {
		this.presenter = presenter;
	}

	@Override
	public void setOwnerName(String name) {
		ownerName.setText(name);
	}

	@UiHandler("join")
	public void onJoin(ClickEvent e) {
		presenter.join(getGameKey());
	}

	@UiHandler("gameKey")
	public void onGameKeyChange(ChangeEvent e) {
		presenter.gameSelected(gameKey.getValue());
	}

	@Override
	public void setJoinable(boolean state) {
		join.setEnabled(state);
	}

	@Override
	public void setObservable(boolean state) {

	}

	@Override
	public void setRows(int rows) {
		this.rows.setText(Integer.toString(rows));
	}

	@Override
	public void setColumns(int columns) {
		this.columns.setText(Integer.toString(columns));
	}

	@Override
	public String getGameKey() {
		return gameKey.getText();
	}

	@Override
	public void setGameKey(String game) {
		gameKey.setText(game);
	}

	@Override
	public void setInformationVisible(boolean state) {
		gameInformation.setVisible(state);
	}
}
