package eu.k5.strategem.client.local.Lobby;

import java.io.IOException;
import java.util.Arrays;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.text.shared.Renderer;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.ValueListBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import eu.k5.strategem.client.local.Lobby.Lobby.StartView;
import eu.k5.strategem.shared.Layout;

@Dependent
public class StartDisplay extends Composite implements StartView {
	@Inject
	UiBinder<VerticalPanel, StartDisplay> binder;

	@UiField
	public Button start;

	@UiField
	public TextBox game;

	@UiField
	public Label ownerName;

	@UiField
	public CheckBox isPublic;

	@UiField(provided = true)
	public ValueListBox<Layout> layoutType;

	private Lobby presenter;

	@Override
	public void setPresenter(Lobby presenter) {
		this.presenter = presenter;

	}

	@PostConstruct
	public void init() {
		layoutType = new ValueListBox<Layout>(new Renderer<Layout>() {

			@Override
			public String render(Layout object) {
				if (object == null) {
					return "";
				}
				return object.getTitle();
			}

			@Override
			public void render(Layout object, Appendable appendable) throws IOException {
				appendable.append(render(object));
			}

		});
		layoutType.setAcceptableValues(Arrays.asList(Layout.values()));

		initWidget(binder.createAndBindUi(this));

	}

	@UiHandler("start")
	public void onStart(ClickEvent e) {
		presenter.start();
	}

	@Override
	public String getName() {
		return game.getValue();
	}

	@Override
	public boolean isPublic() {
		return isPublic.getValue();
	}

	@Override
	public void setOwnerName(String name) {
		ownerName.setText(name);
	}

	@Override
	public Layout getLayout() {
		return layoutType.getValue();
	}
}
