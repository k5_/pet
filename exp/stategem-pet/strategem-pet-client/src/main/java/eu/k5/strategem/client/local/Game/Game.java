package eu.k5.strategem.client.local.Game;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.jboss.errai.bus.client.api.messaging.Message;
import org.jboss.errai.bus.client.api.messaging.MessageBus;
import org.jboss.errai.bus.client.api.messaging.MessageCallback;
import org.jboss.errai.common.client.api.Caller;
import org.jboss.errai.common.client.api.RemoteCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.IsWidget;

import eu.k5.strategem.cell.shared.Decide;
import eu.k5.strategem.cell.shared.MoveTask;
import eu.k5.strategem.cell.shared.SelectFlag;
import eu.k5.strategem.cell.shared.StrategemAspect;
import eu.k5.strategem.client.local.Presenter;
import eu.k5.strategem.client.local.View;
import eu.k5.strategem.client.local.Game.board.Movement;
import eu.k5.strategem.client.local.Game.board.TokenResource;
import eu.k5.strategem.shared.AspectUpdate;
import eu.k5.strategem.shared.Board;
import eu.k5.strategem.shared.GameReference;
import eu.k5.strategem.shared.Location;
import eu.k5.strategem.shared.Player;
import eu.k5.strategem.shared.RequestAspect;
import eu.k5.strategem.shared.StrategemProxy;
import eu.k5.strategem.shared.TokenType;

@Dependent
public class Game implements Presenter {
	private static final Logger LOGGER = LoggerFactory.getLogger(Game.class);

	public interface GameView extends BoardView, SelectView {

		void setOwnerName(String name);

		void setGameName(String name);

		void setOpponentName(String name);

		void displaySelect(boolean state);

		void displayDecide(boolean state);

		void setPlayer(Player player);

		void displayMove(boolean state);

		void displayWon(boolean state);

		void displayLost(boolean state);

		void displayWaiting(boolean state);
	}

	public interface SelectView extends View<Game> {

		void setVisible(boolean state);

	}

	public interface DecideView extends View<Game> {
		void setResources(TokenResource resources);

		void setVisible(boolean state);
	}

	public interface BoardView extends View<Game>, IsWidget {

		void activateMovement(boolean state);

		void initBoard(int rows, int columns);

		void updateTokens(Board board, Player owner);
	}

	@MainView
	@Inject
	private GameView view;

	private GameReference game;
	private StrategemAspect strategem;

	@Inject
	private Caller<StrategemProxy> gems;

	@Inject
	private Event<RequestAspect> requestAspect;

	@Inject
	private MessageBus bus;

	@PostConstruct
	public void init() {
		view.setPresenter(this);
	}

	@Override
	public void display(HasWidgets container) {
		container.clear();

		requestAspect.fire(new RequestAspect(game));
		container.add(view.asWidget());
	}

	public void setGame(GameReference game) {
		this.game = game;
		bus.subscribe("strategemUpdate_" + game.getGame() + "_" + game.getUser(), new MessageCallback() {

			@Override
			public void callback(Message message) {
				System.out.println("Message: " + message);
				onUpdate(message.getValue(StrategemAspect.class));
			}
		});
	}

	public void onAspectChange(@Observes AspectUpdate update) {
		onUpdate(update.getAspect());
	}

	public void onUpdate(StrategemAspect aspect) {
		strategem = aspect;

		view.updateTokens(strategem.getBoard(), strategem.getPlayer());

		view.setPlayer(strategem.getPlayer());
		view.displaySelect(strategem.getSelectFlag() != null);
		view.activateMovement(strategem.getMove() != null);
		view.displayMove(strategem.getMove() != null);
		view.displayDecide(strategem.getDecide() != null);
		view.displayWon(strategem.getWin() != null);
		view.displayLost(strategem.getLost() != null);
		view.displayWaiting(strategem.isWaiting());

		view.setGameName(strategem.getGameName());
		view.setOwnerName(strategem.getOwnerName());
		view.setOpponentName(strategem.getOpponentName());

	}

	public void doSelect(Location location) {
		SelectFlag flag = strategem.getSelectFlag();
		flag.setLocation(location);
		view.displayWaiting(true);
		gems.call(new RemoteCallback<Void>() {

			@Override
			public void callback(Void response) {
				requestAspect.fire(new RequestAspect(game));

			}
		}).selectFlag(game, flag);
	}

	public void doMove(Movement t) {
		MoveTask move = strategem.getMove();
		move.setSource(t.getSource());
		move.setMovement(t.getDirection());
		view.displayWaiting(true);
		gems.call(new RemoteCallback<Void>() {

			@Override
			public void callback(Void response) {
				requestAspect.fire(new RequestAspect(game));

			}
		}).move(game, move);
	}

	public void doDecide(TokenType type) {
		LOGGER.info("{}", type);
		Decide decide = strategem.getDecide();
		decide.setDecision(type);
		view.displayWaiting(true);
		gems.call(new RemoteCallback<Void>() {

			@Override
			public void callback(Void response) {
				// TODO Auto-generated method stub

			}
		}).decide(decide);
	}
}
