package eu.k5.strategem.client.local.Game;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.k5.strategem.client.local.Action;
import eu.k5.strategem.client.local.Game.Game.BoardView;
import eu.k5.strategem.client.local.Game.board.BoardWidget;
import eu.k5.strategem.client.local.Game.board.Movement;
import eu.k5.strategem.shared.Board;
import eu.k5.strategem.shared.Location;
import eu.k5.strategem.shared.Player;
import eu.k5.strategem.shared.Token;

@Dependent
public class BoardDisplay extends BoardWidget implements BoardView {
	private static final Logger LOGGER = LoggerFactory.getLogger(BoardDisplay.class);

	private Action<Token> selectAction;

	Game presenter;

	@Override
	@PostConstruct
	public void init() {
		LOGGER.info("Init board");

		onMovement(new Action<Movement>() {
			@Override
			public void act(Movement t) {
				presenter.doMove(t);
			}
		});
		onTokenClick(new Action<Location>() {

			@Override
			public void act(Location t) {
				presenter.doSelect(t);
			}
		});

	}

	@Override
	public void setPresenter(Game move) {
		presenter = move;
	}

	@Override
	public void updateTokens(Board board, Player owner) {
		setBoard(board, owner);
	}

	@Override
	public void activateMovement(boolean state) {
		setMovementActive(state);
	}

}
