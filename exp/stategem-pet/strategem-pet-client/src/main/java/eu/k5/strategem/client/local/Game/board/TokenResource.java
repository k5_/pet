package eu.k5.strategem.client.local.Game.board;

import com.google.gwt.resources.client.ImageResource;

public interface TokenResource {
	ImageResource none();

	ImageResource unspecified();

	ImageResource lizard();

	ImageResource spock();

	ImageResource paper();

	ImageResource rock();

	ImageResource scissors();

	ImageResource flag();

	ImageResource capturedFlag();

	ImageResource battle();
}
