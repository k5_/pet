package eu.k5.strategem.client.local.Game.tokens.blue;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

import eu.k5.strategem.client.local.Game.board.TokenResource;

public interface BlueTokenResource extends TokenResource, ClientBundle {

	BlueTokenResource INSTANCE = GWT.create(BlueTokenResource.class);

	@Override
	@Source("lizard.png")
	ImageResource lizard();

	@Override
	@Source("spock.png")
	ImageResource spock();

	@Override
	@Source("paper.png")
	ImageResource paper();

	@Override
	@Source("rock.png")
	ImageResource rock();

	@Override
	@Source("scissors.png")
	ImageResource scissors();

	@Override
	@Source("1px.png")
	ImageResource none();

	@Override
	@Source("flag.png")
	ImageResource flag();

	@Override
	@Source("capturedFlag.png")
	ImageResource capturedFlag();

	@Override
	@Source("unspecified.png")
	ImageResource unspecified();

	@Override
	@Source("battle.png")
	ImageResource battle();

}
