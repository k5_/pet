package eu.k5.strategem;

import java.io.StringReader;

import javax.xml.bind.JAXB;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Jax {

	String test;

	public String getTet() {
		return test;
	}

	public void setTet(String test) {
		this.test = test;
	}

	public static void main(String[] args) {
		Jax unmarshal = JAXB.unmarshal(new StringReader("<jax><tet>abc</tet></jax>"), Jax.class);
		System.out.println(unmarshal.test);
	}
}
