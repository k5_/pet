package eu.k5.strategem.client.local.Game.board;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface Movements extends ClientBundle {

	Movements INSTANCE = GWT.create(Movements.class);

	@Source("noneVertical.png")
	ImageResource noneVertical();

	@Source("noneHorizontal.png")
	ImageResource noneHorizontal();

	@Source("moveUp.png")
	ImageResource moveUp();

	@Source("moveDown.png")
	ImageResource moveDown();

	@Source("moveRight.png")
	ImageResource moveRight();

	@Source("moveLeft.png")
	ImageResource moveLeft();

}
