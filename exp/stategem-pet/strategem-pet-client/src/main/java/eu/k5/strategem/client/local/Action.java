package eu.k5.strategem.client.local;

public interface Action<T> {

	void act(T t);
}
