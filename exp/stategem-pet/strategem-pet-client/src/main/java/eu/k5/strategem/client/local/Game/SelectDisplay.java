package eu.k5.strategem.client.local.Game;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.VerticalPanel;

import eu.k5.strategem.client.local.Game.Game.SelectView;

@Dependent
public class SelectDisplay extends Composite implements SelectView {

	@Inject
	UiBinder<VerticalPanel, SelectDisplay> binder;
	private Game presenter;

	@PostConstruct
	public void init() {
		initWidget(binder.createAndBindUi(this));
	}

	public void display(HasWidgets container) {
		container.add(asWidget());
	}

	@Override
	public void setPresenter(Game presenter) {
		this.presenter = presenter;

	}
}
