package eu.k5.strategem.client.local.Game;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.VerticalPanel;

import eu.k5.strategem.client.local.Game.Game.DecideView;
import eu.k5.strategem.client.local.Game.board.TokenResource;
import eu.k5.strategem.shared.TokenType;

public class Decide extends Composite implements DecideView {

	@Inject
	UiBinder<VerticalPanel, Decide> binder;

	@UiField
	public Image rock;

	@UiField
	public Image paper;

	@UiField
	public Image scissors;

	// @UiField
	// public Image lizard;
	//
	// @UiField
	// public Image spock;

	private Game presenter;

	@PostConstruct
	public void init() {
		initWidget(binder.createAndBindUi(this));
	}

	@Override
	public void setPresenter(Game presenter) {
		this.presenter = presenter;
	}

	@Override
	public void setResources(TokenResource resource) {
		rock.setResource(resource.rock());
		scissors.setResource(resource.scissors());
		paper.setResource(resource.paper());
		// lizard.setResource(resource.lizard());
		// spock.setResource(resource.spock());
	}

	@UiHandler("rock")
	public void onRock(ClickEvent e) {
		presenter.doDecide(TokenType.ROCK);
	}

	@UiHandler("paper")
	public void onPaper(ClickEvent e) {
		presenter.doDecide(TokenType.PAPER);
	}

	@UiHandler("scissors")
	public void onScissors(ClickEvent e) {
		presenter.doDecide(TokenType.SCISSORS);
	}

	// @UiHandler("lizard")
	// public void onLizard(ClickEvent e) {
	// presenter.doDecide(TokenType.LIZARD);
	// }
	//
	// @UiHandler("spock")
	// public void onSpock(ClickEvent e) {
	// presenter.doDecide(TokenType.SPOCK);
	// }

}
