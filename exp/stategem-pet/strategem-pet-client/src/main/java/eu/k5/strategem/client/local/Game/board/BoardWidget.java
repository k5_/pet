package eu.k5.strategem.client.local.Game.board;

import java.util.EnumSet;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;

import org.jboss.errai.ui.nav.client.local.HistoryToken;

import com.google.gwt.user.client.ui.Grid;

import eu.k5.strategem.client.local.Action;
import eu.k5.strategem.client.local.Game.tokens.blue.BlueTokenResource;
import eu.k5.strategem.client.local.Game.tokens.red.RedTokenResource;
import eu.k5.strategem.shared.Board;
import eu.k5.strategem.shared.Direction;
import eu.k5.strategem.shared.Location;
import eu.k5.strategem.shared.Player;
import eu.k5.strategem.shared.Token;
import eu.k5.strategem.shared.TokenType;

@Dependent
public class BoardWidget extends Grid {

	private TokenWidget[][] widgets;
	private Action<Location> selectAction;
	private Action<Movement> moveAction;

	private int rows;
	private int columns;

	public BoardWidget() {

	}

	@PostConstruct
	public void init() {

	}

	private void setSize(int rows, int columns) {
		this.rows = rows;
		this.columns = columns;
		resize(rows, columns);
		widgets = new TokenWidget[rows][columns];
	}

	public void initBoard(int rows, int columns) {
		if (this.rows == rows && this.columns == columns) {
			return;
		}

		setSize(rows, columns);

		for (int row = 0; row < rows; row++) {
			for (int column = 0; column < columns; column++) {
				final Location location = new Location(row, column);
				TokenWidget current = new TokenWidget(RedTokenResource.INSTANCE, BlueTokenResource.INSTANCE,
						Movements.INSTANCE);

				current.setMoveAction(new Action<Direction>() {

					@Override
					public void act(Direction t) {
						System.out.println("Row: " + location.getRow() + " Column: " + location.getColumn()
								+ " Movement " + t);
						moveAction.act(new Movement(location, t));
					}
				});

				current.setSelectAction(new Action<Void>() {
					@Override
					public void act(Void t) {
						selectAction.act(location);
					}
				});

				current.setToken(TokenType.NONE, false, EnumSet.noneOf(Direction.class));

				widgets[row][column] = current;
				setWidget(row, column, current);
			}
		}
		setBorderWidth(1);
	}

	public void setBoard(Board board, Player player) {
		initBoard(board.getRows(), board.getColumns());

		for (int row = 0; row < board.getRows(); row++) {
			for (int column = 0; column < board.getColumns(); column++) {
				Token token = board.getTokenAt(row, column);
				if (token == null) {
					widgets[row][column].setToken(TokenType.NONE, false, EnumSet.noneOf(Direction.class));
				} else if (token.getOwner().equals(player)) {

					widgets[row][column]
							.setToken(token.getType(), true, calculateMovements(board, player, row, column));
				} else {
					if (board.isLastBattle(row, column)) {
						widgets[row][column].setToken(token.getType(), false, EnumSet.noneOf(Direction.class));
					} else {
						widgets[row][column].setToken(TokenType.UNSPECIFIED, false, EnumSet.noneOf(Direction.class));
					}
				}
			}
		}
	}

	private Set<Direction> calculateMovements(Board board, Player player, int row, int column) {
		Set<Direction> allowed = EnumSet.noneOf(Direction.class);
		if (board.getTokenAt(row, column).getType().equals(TokenType.FLAG)) {
			return allowed;
		}

		if (board.isValid(row + 1, column) && !player.equals(board.getOwner(row + 1, column))) {
			allowed.add(Direction.DOWN);
		}
		if (board.isValid(row - 1, column) && !player.equals(board.getOwner(row - 1, column))) {
			allowed.add(Direction.UP);
		}
		if (board.isValid(row, column + 1) && !player.equals(board.getOwner(row, column + 1))) {
			allowed.add(Direction.RIGHT);
		}
		if (board.isValid(row, column - 1) && !player.equals(board.getOwner(row, column - 1))) {
			allowed.add(Direction.LEFT);
		}

		return allowed;
	}

	public void setMovementActive(boolean state) {
		for (TokenWidget[] row : widgets) {
			for (TokenWidget widget : row) {
				if (widget != null) {
					widget.setMovementActive(state);
				}
			}
		}
	}

	public void onTokenClick(Action<Location> action) {
		selectAction = action;
	}

	public void onMovement(Action<Movement> action) {
		moveAction = action;
	}
}
