package eu.k5.strategem.client.local.Game;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;

import eu.k5.strategem.client.local.Game.Game.BoardView;
import eu.k5.strategem.client.local.Game.Game.DecideView;
import eu.k5.strategem.client.local.Game.Game.GameView;
import eu.k5.strategem.client.local.Game.Game.SelectView;
import eu.k5.strategem.client.local.Game.tokens.blue.BlueTokenResource;
import eu.k5.strategem.shared.Board;
import eu.k5.strategem.shared.Player;

@MainView
@Dependent
public class GameDisplay extends Composite implements GameView {

	@Inject
	UiBinder<HTMLPanel, GameDisplay> binder;

	@UiField(provided = true)
	@Inject
	public BoardView board;

	@UiField(provided = true)
	@Inject
	public DecideView decide;

	@UiField
	public Label won;

	@UiField
	public Label lost;

	@UiField
	public Label gameName;

	@UiField
	public Label ownerName;

	@UiField
	public Label opponentName;

	@UiField
	public Label move;
	@UiField
	public Label waiting;

	@UiField(provided = true)
	@Inject
	public SelectView select;

	@PostConstruct
	public void init() {
		initWidget(binder.createAndBindUi(this));
		decide.setVisible(false);
		select.setVisible(false);
	}

	@Override
	public void setPresenter(Game presenter) {
		board.setPresenter(presenter);
		decide.setPresenter(presenter);
		select.setPresenter(presenter);
	}

	@Override
	public void activateMovement(boolean state) {
		board.activateMovement(state);
	}

	@Override
	public void initBoard(int rows, int columns) {
		board.initBoard(rows, columns);
	}

	@Override
	public void updateTokens(Board board, Player owner) {
		this.board.updateTokens(board, owner);
	}

	@Override
	public void displaySelect(boolean state) {
		select.setVisible(state);
	}

	@Override
	public void setPlayer(Player player) {
		decide.setResources(BlueTokenResource.INSTANCE);
	}

	@Override
	public void displayDecide(boolean state) {
		decide.setVisible(state);
	}

	@Override
	public void displayWon(boolean state) {
		won.setVisible(state);
	}

	@Override
	public void displayLost(boolean state) {
		lost.setVisible(state);
	}

	@Override
	public void displayWaiting(boolean state) {
		waiting.setVisible(state);
	}

	@Override
	public void setOwnerName(String name) {
		ownerName.setText(name);
	}

	@Override
	public void setGameName(String name) {
		gameName.setText(name);
	}

	@Override
	public void setOpponentName(String name) {
		opponentName.setText(name);
	}

	@Override
	public void displayMove(boolean state) {
		move.setVisible(state);
	}
}
