package eu.k5.strategem.client.local;

import javax.inject.Inject;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.RootLayoutPanel;

import eu.k5.strategem.client.local.Game.Game;
import eu.k5.strategem.client.local.Lobby.Lobby;
import eu.k5.strategem.shared.GameReference;

public class AppController implements ValueChangeHandler<String> {

	@Inject
	Game game;

	@Inject
	Lobby lobby;

	@Inject
	Navigator navigator;

	public void go(final RootLayoutPanel panel) {
		String pageName = navigator.getPageName();

		if ("game".equals(pageName)) {
			GameReference ref = reference();
			game.setGame(ref);
			game.display(panel);

		} else {
			lobby.display(panel);
		}
	}

	private GameReference reference() {
		String game = navigator.getParameter(ClientConstants.PARAM_GAME);
		String player = navigator.getParameter(ClientConstants.PARAM_USER);
		boolean owner = "true".equals(navigator.getParameter("owner"));

		return new GameReference(game, player, owner);
	}

	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		go(RootLayoutPanel.get());
	}

}
