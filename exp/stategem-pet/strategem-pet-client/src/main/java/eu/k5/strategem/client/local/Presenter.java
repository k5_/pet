package eu.k5.strategem.client.local;

import com.google.gwt.user.client.ui.HasWidgets;

public interface Presenter {

	void display(HasWidgets container);
}
