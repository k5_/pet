package eu.k5.strategem.client.local;

import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;

public interface View<P extends Presenter> extends IsWidget {

	void setPresenter(P presenter);

	@Override
	Widget asWidget();
}
