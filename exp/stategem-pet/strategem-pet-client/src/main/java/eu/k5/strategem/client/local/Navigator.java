package eu.k5.strategem.client.local;

import java.util.Collection;

import javax.enterprise.context.Dependent;

import org.jboss.errai.ui.nav.client.local.HistoryToken;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.ImmutableMultimap.Builder;
import com.google.gwt.user.client.History;

@Dependent
public class Navigator {

	public static class Parameter {
		private final String name;
		private final String value;

		public Parameter(String name, String value) {
			this.name = name;
			this.value = value;
		}

		public String getName() {
			return name;
		}

		public String getValue() {
			return value;
		}
	}

	public String getParameter(String name) {
		HistoryToken token = HistoryToken.parse(History.getToken());
		Collection<String> states = token.getState().get(name);
		if (states.isEmpty()) {
			return null;
		} else {
			return states.iterator().next();
		}
	}

	public void toDefault() {
		to("", new String[0]);
	}

	public void to(String pageName, Parameter... parameters) {
		Builder<String, String> builder = ImmutableMultimap.builder();
		for (Parameter p : parameters) {
			builder.put(p.getName(), p.getValue());
		}

		HistoryToken target = HistoryToken.of(pageName, builder.build());
		History.newItem(target.toString(), true);
	}

	public void to(String pageName, String... parameters) {
		Builder<String, String> builder = ImmutableMultimap.builder();
		for (int i = 0; i < parameters.length; i += 2) {
			builder.put(parameters[i], parameters[i + 1]);
		}
		HistoryToken target = HistoryToken.of(pageName, builder.build());
		History.newItem(target.toString(), true);
	}

	public String getPageName() {
		HistoryToken token = HistoryToken.parse(History.getToken());
		return token.getPageName();
	}
}
