package eu.k5.strategem.client.local.Lobby;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

import eu.k5.strategem.client.local.Lobby.Lobby.JoinView;
import eu.k5.strategem.client.local.Lobby.Lobby.LobbyView;
import eu.k5.strategem.client.local.Lobby.Lobby.StartView;
import eu.k5.strategem.shared.StrategemPublic;

@Dependent
public class LobbyDisplay extends Composite implements LobbyView {

	@Inject
	UiBinder<DockLayoutPanel, LobbyDisplay> binder;

	private Lobby presenter;

	@Inject
	@UiField(provided = true)
	public JoinView join;

	@Inject
	@UiField(provided = true)
	public StartView start;

	@UiField(provided = true)
	public DataGrid<StrategemPublic> activeGames;

	@PostConstruct
	public void init() {

		activeGames = new DataGrid<StrategemPublic>();

		activeGames.addColumn(new TextColumn<StrategemPublic>() {
			@Override
			public String getValue(StrategemPublic gem) {
				return gem.getGame();
			}
		}, "Game");

		activeGames.addColumn(new TextColumn<StrategemPublic>() {
			@Override
			public String getValue(StrategemPublic gem) {
				return gem.getGameName();
			}
		}, "Name");

		activeGames.addColumn(new TextColumn<StrategemPublic>() {
			@Override
			public String getValue(StrategemPublic gem) {
				return gem.getOwnerName();
			}
		}, "Owner");

		activeGames.addColumn(new TextColumn<StrategemPublic>() {
			@Override
			public String getValue(StrategemPublic gem) {
				return gem.getOpponentName();
			}
		}, "Opponent");

		activeGames.addColumn(new TextColumn<StrategemPublic>() {
			@Override
			public String getValue(StrategemPublic gem) {
				return gem.getRows() + "x" + gem.getColumns();
			}
		}, "Layout");

		final SingleSelectionModel<StrategemPublic> selectionModel = new SingleSelectionModel<StrategemPublic>();
		activeGames.setSelectionModel(selectionModel);
		selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
			@Override
			public void onSelectionChange(SelectionChangeEvent event) {
				StrategemPublic selected = selectionModel.getSelectedObject();
				if (selected != null) {
					presenter.setJoinData(selected);
				}
			}
		});
		initWidget(binder.createAndBindUi(this));
	}

	@Override
	public void setPresenter(Lobby lobby) {
		presenter = lobby;
		join.setPresenter(lobby);
		start.setPresenter(lobby);
	}

	@Override
	public JoinView getJoin() {
		return join;
	}

	@Override
	public StartView getStart() {
		return start;
	}

	@Override
	public void setGames(List<StrategemPublic> games) {
		activeGames.setRowData(games);
	}

}
