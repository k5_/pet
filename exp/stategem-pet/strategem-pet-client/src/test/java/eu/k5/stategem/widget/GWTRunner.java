package eu.k5.stategem.widget;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;

import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.InitializationError;

public class GWTRunner extends BlockJUnit4ClassRunner {
	public GWTRunner(Class<?> type) throws InitializationError {
		super(getFromTestClassloader(type));
	}

	public static class GWT {

		public static <T> T create(Class<T> type) {
			return null;
		}
	}

	static class ReplaceClassLoader extends URLClassLoader {
		public ReplaceClassLoader() {
			super(((URLClassLoader) getSystemClassLoader()).getURLs());
		}

		@Override
		protected Class<?> findClass(String name) throws ClassNotFoundException {
			System.out.println(name);

			return super.findClass(name);
		}

		@Override
		protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
			System.out.println(name);

			// TODO Auto-generated method stub
			return super.loadClass(name, resolve);
		}

		@Override
		public Class<?> loadClass(String name) throws ClassNotFoundException {
			System.out.println(name);
			if ("com.google.gwt.core.client.GWT".equals(name)) {
				return super.findClass(name);
			}
			if (name.startsWith("eu.k5.stategem.widget") || name.startsWith("com.google.gwt.user.client")) {
				return super.findClass(name);
			}
			// TODO Auto-generated method stub
			return super.loadClass(name);
		}

		@Override
		public InputStream getResourceAsStream(String name) {
			System.out.println(name);

			// TODO Auto-generated method stub
			return super.getResourceAsStream(name);
		}

		@Override
		public URL getResource(String name) {
			System.out.println(name);

			// TODO Auto-generated method stub
			return super.getResource(name);
		}

		@Override
		public Enumeration<URL> getResources(String name) throws IOException {
			System.out.println(name);

			// TODO Auto-generated method stub
			return super.getResources(name);
		}
	}

	private static Class<?> getFromTestClassloader(Class<?> clazz) throws InitializationError {
		try {
			ClassLoader testClassLoader = new ReplaceClassLoader();
			return Class.forName(clazz.getName(), true, testClassLoader);
		} catch (ClassNotFoundException e) {
			throw new InitializationError(e);
		}
	}

}
