package eu.k5.strategem.client.local.Flag;

import com.google.gwt.user.client.ui.HasWidgets;

import eu.k5.strategem.cell.shared.SelectFlag;
import eu.k5.strategem.client.local.Action;
import eu.k5.strategem.client.local.View;
import eu.k5.strategem.shared.Board;
import eu.k5.strategem.shared.GameReference;
import eu.k5.strategem.shared.Token;

public interface SelectFlagView extends View {

	void setTokens(Board board);

	void setSelect(Action<Token> selectAction);

	void setTask(SelectFlag selectFlag);

	SelectFlag getTask();

	void setGame(GameReference game);

	GameReference getGame();

	void display(HasWidgets container);

	void selectedAction(Action<Void> action);
}
