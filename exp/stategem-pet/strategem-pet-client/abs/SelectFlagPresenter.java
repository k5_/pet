package eu.k5.strategem.client.local.Flag;

import javax.inject.Inject;

import org.jboss.errai.common.client.api.Caller;
import org.jboss.errai.common.client.api.RemoteCallback;

import eu.k5.strategem.cell.shared.SelectFlag;
import eu.k5.strategem.client.local.Navigator;
import eu.k5.strategem.shared.Location;
import eu.k5.strategem.shared.StrategemProxy;

public class SelectFlagPresenter {

	SelectFlagView view;

	@Inject
	Caller<StrategemProxy> strategems;

	@Inject
	Navigator navigator;

	public void init(SelectFlagView view) {
		this.view = view;
	}

	public void selectFlag(Location t) {
		SelectFlag task = view.getTask();
		task.setLocation(t);
		strategems.call(new RemoteCallback<Void>() {

			@Override
			public void callback(Void response) {
				// TODO Auto-generated method stub

			}
		}).selectFlag(view.getGame(), task);

	}

}
