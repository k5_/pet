package eu.k5.strategem.client.local.Flag;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HasWidgets;

import eu.k5.strategem.cell.shared.SelectFlag;
import eu.k5.strategem.client.local.Action;
import eu.k5.strategem.client.local.widgets.BoardWidget;
import eu.k5.strategem.shared.Board;
import eu.k5.strategem.shared.GameReference;
import eu.k5.strategem.shared.Location;
import eu.k5.strategem.shared.Player;
import eu.k5.strategem.shared.Token;

@Dependent
public class SelectFlagDisplay extends Composite implements SelectFlagView {

	@Inject
	UiBinder<HTMLPanel, SelectFlagDisplay> binder;

	@Inject
	SelectFlagPresenter presenter;

	@UiField(provided = true)
	public BoardWidget board;

	private Action<Token> selectAction;

	private SelectFlag task;
	private GameReference game;

	@Override
	public void setSelect(Action<Token> selectAction) {
		this.selectAction = selectAction;
	}

	@PostConstruct
	public void init() {

		board = new BoardWidget();

		initWidget(binder.createAndBindUi(this));
		presenter.init(this);
		board.onTokenClick(new Action<Location>() {

			@Override
			public void act(Location t) {
				presenter.selectFlag(t);
			}
		});

	}

	@Override
	public void setTokens(Board board) {
		this.board.setBoard(board, Player.OPPONENT);
	}

	@Override
	public void setTask(SelectFlag selectFlag) {
		task = selectFlag;
	}

	@Override
	public void display(HasWidgets container) {
		container.add(this.asWidget());
	}

	@Override
	public SelectFlag getTask() {
		return task;
	}

	@Override
	public GameReference getGame() {
		return game;
	}

	@Override
	public void setGame(GameReference game) {
		this.game = game;
	}
}
