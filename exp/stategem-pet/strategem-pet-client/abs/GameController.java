package eu.k5.strategem.client.local;

import javax.inject.Inject;

import com.google.gwt.user.client.ui.HasWidgets;

import eu.k5.strategem.cell.shared.StrategemView;
import eu.k5.strategem.client.local.Flag.SelectFlagView;
import eu.k5.strategem.client.local.Move.Move;
import eu.k5.strategem.shared.GameReference;

public class GameController {

	@Inject
	SelectFlagView selectFlag;

	@Inject
	Move move;

	public void go(HasWidgets container, GameReference game, StrategemView strategem) {

		if (strategem.getSelectFlag() != null) {

			selectFlag.setTokens(strategem.getData().getBoard());
			selectFlag.setTask(strategem.getSelectFlag());
			selectFlag.setGame(game);
			selectFlag.display(container);
			selectFlag.selectedAction(new Action<Void>() {

				@Override
				public void act(Void t) {
					// TODO Auto-generated method stub

				}
			});

		} else if (strategem.getMove() != null) {
			move.setGame(game);
			move.setStrategem(strategem);

			move.display(container);
			move.movedAction(new Action<Void>() {

				@Override
				public void act(Void t) {
					// TODO Auto-generated method stub

				}
			});
			// System.out.println("move");
		}

		// move.setTokens(strategem.getData().getBoard());
		//
		// } else {
		System.out.println("invalid state");
		// }

	}
}
